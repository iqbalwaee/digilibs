<?php
namespace App\Persistant;
use AuditStash\PersisterInterface;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;
use Cake\Network\Request;

class DatabasePersister implements PersisterInterface
{
    public function logEvents(array $auditLogs)
    {
		$this->request = new Request();
		$this->request->trustProxy = true;
		$ipAddress = $this->request->clientIp();
		$hostname = gethostname();
		$browserInfo = $this->request->header('User-Agent');;
        foreach ($auditLogs as $log) {
            $eventType = $log->getEventType();
			$metaInfo = $log->getMetaInfo();
            $data = [
                'timestamp' => $log->getTimestamp(),
                'transaction' => $log->getTransactionId(),
                'type' => $log->getEventType(),
                'primary_key' => $log->getId(),
                'source' => $log->getSourceName(),
                'parent_source' => $log->getParentSourceName(),
                'changed' => $eventType === 'delete' ? null : json_encode($log->getChanged()),
                'ip_address' => $log->getParentSourceName(),
                'meta' => json_encode($metaInfo),
				'ip_address'=>$ipAddress,
				'browser_info'=>$browserInfo,
				'pc_name'=>$hostname,
            ];
            if(strpos($_SERVER['REQUEST_URI'],"stadmin")){
                TableRegistry::get('Audits')->save(new Entity($data));
            }
            
        }
    }
}