<?php
namespace App\Auth;

use Cake\Auth\AbstractPasswordHasher;
use Cake\Utility\Security;

class StPasswordHasher extends AbstractPasswordHasher
{

    public function hash($password)
    {
        return Security::hash($password,'sha1',true);
    }
	
	public function hashBlowFish($password)
    {
        return Security::hash($password,'blowfish',false);
    }

    public function check($password, $hashedPassword)
    {
        return Security::hash($password,'sha1',true) === $hashedPassword;
    }
}