<?php
/* src/View/Helper/LinkHelper.php */
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Utility\Inflector;

class UtilitiesHelper extends Helper
{
	public $helpers = ['Url','Html','Form'];

	public function statusActive($key = null)
    {
        $array = ['<span class="label label-sm label-danger"> Inactive </span>','<span class="label label-sm label-success"> Active </span>'];
        if($key == null && $key != 0){
            return $array;
        }else{
            return $array[$key];
        }
    }
    public function statusYes($key = null)
    {
        $array = ['<span class="label label-sm label-danger"> No </span>','<span class="label label-sm label-success"> Yes </span>'];
        if($key == null && $key != 0){
            return $array;
        }else{
            return $array[$key];
        }
    }
	
	public function generateUrlImage($img_dir,$img,$prefix = null,$img_not_found = null){
		//full_path_dir
        $baseDir = "webroot/resources/";
        $img_dir = $baseDir.$img_dir."/";
        $img = $prefix.$img;
		$changeSlash 		= str_replace("\\",DS,$img_dir);
		$changeSlash		= str_replace("/",DS,$changeSlash); 
		$full_path = ROOT.DS.$changeSlash.$img;
		//check image exist
		if(file_exists($full_path)){
			$dir 		= str_replace("\\","/",$img_dir).$img;
			$url = $this->Url->build("/".$dir,true);
		}else{
			if($img_not_found == null){
				$url = $this->Url->build("/img/not-found.png",true);
			}else{
				$url = $this->Url->build("/".$img_not_found,true);
			}
		}
		return $url;
	}

    public function yesNo($key = null)
    {
	$array = ['<span class="label label-sm label-danger"> No </span>','<span class="label label-sm label-success"> Yes </span>'];

        if($key == null && $key != 0){
            return $array;
        }else{
            return $array[$key];
        }
    }
    
    public function inflectorHumanize($string){
        $string_low = strtolower($string);
        return $inflec = Inflector::slug($string_low);
    }

}