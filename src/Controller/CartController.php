<?php
namespace App\Controller;

use App\Controller\AppController;

class CartController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['addToCart','removeFromCart','index']);
    }
    
    public function index()
    {
        $session = $this->request->session();
        $carts    = $session->read('Cart');
        $countCart = count($carts);
        $this->set(compact('carts','countCart'));
    }
    
    public function addToCart(){
        $this->autoRender = false;
        if($this->request->is(['post','ajax'])){
            $catalogId = $this->request->data['catalog_id'];
            $this->loadModel('Catalogs');
            $catalog = $this->Catalogs->get($catalogId,['contain'=>'Members']);
            $session = $this->request->session();
            $cart    = $session->read('Cart');
            $session->write('Cart.'.$catalogId  , $catalog);
            $cart    = $session->read('Cart');
            $countCart = count($cart);
            $this->response->body(json_encode(['errCode' => 0,'errMsg'=>'','countCart' => $countCart]));
        }
    }
    
    public function removeFromCart(){
        $this->autoRender = false;
        if($this->request->is(['post','ajax'])){
            $catalogId = $this->request->data['catalog_id'];
            $session = $this->request->session();
            $session->delete('Cart.'.$catalogId);
            $cart    = $session->read('Cart');
            $countCart = count($cart);
            $this->response->body(json_encode(['errCode' => 0,'errMsg'=>'','countCart' => $countCart]));
        }
    }
}

