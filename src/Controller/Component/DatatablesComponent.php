<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Network\Request;
use Cake\View\View;

class DatatablesComponent extends Component
{
    public $controller = null;

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
    }
    
    public function paginate($key = null){
        if($this->controller->request->isAjax){
            $request = $this->request->data;
            $draw = $request['draw'];
            $model = $this->config[$key];
            $tableRegistry = TableRegistry::get($key);
            $start = $request['start'];
            $config = [];
            if(!empty($model['contain'])){
                $config = $config + ['contain' => $model['contain']];
            }
            if($request['length'] != -1){
                $config = $config + ['limit' => $request['length'],'page' => ($request['start'] == 0 ? 1 : ceil($request['start'] / $request['length']) + 1 )];
            }
            $ordering = $this->getSort();
            if($ordering){
                $config = $config + ['order' => $ordering];
            }
            $searching = $this->getSearch();
            if($searching){
                $config = $config + ['conditions' => 
                                     ['OR' => $searching]];
            }
            //pr($config);
            $data = $tableRegistry->find('all',$config);
            $totaldata = $data->count();
            $totalfiltered = $totaldata;
            $result = array(
                "draw"            => intval( $draw ),
                "recordsTotal"    => intval( $totaldata ),
                "recordsFiltered" => intval( $totalfiltered ),
                "data"            => $data
            );
            
            $_serialize = false;
            $this->controller->set(compact('result','_serialize','start'));
            $this->controller->autoRender = false;
            $this->controller->render('datatables/'.$this->controller->request->action);
        }
        return false;
    }
    
    public function getSort(){
        $request = $this->request->data;
        $cols = $request['columns'];
        $orders = $request['order'];
        $datasort = false;
        foreach($orders as $key => $order){
            if($cols[$order['column']]['orderable'] == "true"){
                $datasort[$cols[$order['column']]['name']] = $order['dir'];
            }
        }
        return $datasort;
    }
    
    public function getSearch(){
        $request = $this->request->data;
        $cols = $request['columns'];
        $search = $request['search']['value'];
        $condition = [];
        if(!empty($search)){
            foreach($cols as $key => $col){
                if($col['searchable'] != "false"){
					$condition += [$col['name']." LIKE" => "%".$search."%"];
                }
            }
        }
        return $condition;
    }
    
}