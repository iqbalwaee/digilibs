<?php
namespace App\Controller\Stadmin;

use App\Controller\AppController;

/**
 * Catalogs Controller
 *
 * @property \App\Model\Table\CatalogsTable $Catalogs
 */
class CatalogsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->Datatables->config = [
            'Catalogs' => ['contain'=>['Members']]
        ];
        $catalogs = $this->Datatables->paginate('Catalogs');
        $this->set(compact('catalogs'));
    }

    /**
     * View method
     *
     * @param string|null $id Catalog id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $catalog = $this->Catalogs->get($id, [
            'contain' => ['Members']
        ]);
        $this->set('catalog', $catalog);
        $this->set('_serialize', ['catalog']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $catalog = $this->Catalogs->newEntity();
        if ($this->request->is('post')) {
            $catalog = $this->Catalogs->patchEntity($catalog, $this->request->data);
            if ($this->Catalogs->save($catalog)) {
                $this->Flash->success(__('The catalog has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The catalog could not be saved. Please, try again.'));
            }
        }
        $members = $this->Catalogs->Members->find('list', ['limit' => 200]);
        $this->set(compact('catalog', 'members'));
        $this->set('_serialize', ['catalog']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Catalog id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $catalog = $this->Catalogs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $catalog = $this->Catalogs->patchEntity($catalog, $this->request->data);
            if ($this->Catalogs->save($catalog)) {
                $this->Flash->success(__('The catalog has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The catalog could not be saved. Please, try again.'));
            }
        }
        $members = $this->Catalogs->Members->find('list', ['limit' => 200]);
        $this->set(compact('catalog', 'members'));
        $this->set('_serialize', ['catalog']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Catalog id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $catalog = $this->Catalogs->get($id);
        if ($this->Catalogs->delete($catalog)) {
            $this->Flash->success(__('The catalog has been deleted.'));
        } else {
            $this->Flash->error(__('The catalog could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
