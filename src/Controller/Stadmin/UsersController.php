<?php
namespace App\Controller\Stadmin;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->Datatables->config = [
            'Users' => ['contain' => ['Groups']]
        ];
        $users = $this->Datatables->paginate('Users');
        $this->set(compact('users'));
    }
    
    public function configure($id = null){
        $this->loadModel('Acos');
        $user = $this->Users->get($id, [
            'contain' => ['Groups.Aros','Aros']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            $this->Acl->deny(['model' => 'Users', "foreign_key"=>$id],'controllers');
            foreach($data['aros_acos'] as $key => $val){
                $findPath = $this->Acos->find('path',['for'=>$val['aco_id']]);
                $url = "";
                foreach($findPath as $as => $r){
                   $url .= $r->alias."/";
                }
                $urlAco = substr($url,0,-1);
                if($val['allow']==1){
                    $this->Acl->allow(['model' => 'Users', "foreign_key"=>$id],$urlAco);
                }else{
                    $this->Acl->deny(['model' => 'Users', "foreign_key"=>$id],$urlAco);
                }
            }
            $this->autoRender = false;
            $this->Flash->success(__('The User has been configured.'));
            return $this->redirect(['action' => 'index']);
        }
        $listAcos = $this->Acos->find('children', [
			'for' => 5,
			'contain'=>[
				'ParentAcos',
				'ArosAcos'=>[
					'conditions'=>[
						'ArosAcos.aro_id' => $user->group->aro->id,
						'ArosAcos._create' => 1
					]
				]
			]
		])->toArray();
		$this->loadModel('ArosAcos');
		$acos = [];
		foreach($listAcos as $key => $aco){
			$findAroAco = $this->ArosAcos->find('all',[
				'conditions' => [
					'aco_id' => $aco->id,
					'aro_id' => $user->aro->id,
					'_create' => 1
				]
			])->count();
			$isActive = false;
			if($findAroAco){
				$isActive = true;
			}
			$aco->is_active = $isActive;
			$acos[$key] = $aco;
		}
        $this->set('user', $user);
        $this->set('acos', $acos);
    }
    
    
    /**
    * Login Method
    * @return \Cake\Network\Response|null
    */
    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect(['controller'=>'dashboard','action' => 'index','prefix'=>'stadmin']);
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }
    
    /** 
    * Logout Method
    */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Groups']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $groups = $this->Users->Groups->find('list', ['limit' => 200]);
        $this->set(compact('user', 'groups'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $groups = $this->Users->Groups->find('list', ['limit' => 200]);
        $this->set(compact('user', 'groups'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
