<?php
namespace App\Controller\Stadmin;

use App\Controller\AppController;

/**
 * Groups Controller
 *
 * @property \App\Model\Table\GroupsTable $Groups
 */
class GroupsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index','configure']);
    }
    
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->Datatables->config = [
            'Groups' => []
        ];
        $acos = $this->Datatables->paginate('Groups');
        $this->set(compact('groups'));
    }

    /**
     * View method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $group = $this->Groups->get($id, [
            
        ]);

        $this->set('group', $group);
        $this->set('_serialize', ['group']);
    }
    
    public function configure($id = null){
        $this->loadModel('Acos');
        $this->loadModel('Aros');
        $group = $this->Groups->get($id, [
            'contain' => ['Users','Aros']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            $this->Acl->deny(['model' => 'Groups', "foreign_key"=>$id],'controllers');
			$childs = $this->Aros->find('children', ['for' => $group->aro->id])->toArray();
            foreach($data['aros_acos'] as $key => $val){
                $findPath = $this->Acos->find('path',['for'=>$val['aco_id']]);
                $url = "";
                foreach($findPath as $as => $r){
                   $url .= $r->alias."/";
                }
                $urlAco = substr($url,0,-1);
                if($val['allow']==1){
                    $this->Acl->allow(['model' => 'Groups', "foreign_key"=>$id],$urlAco);
					foreach($childs as $skey => $child){
						$this->Acl->allow(['model' => 'Users', "foreign_key"=>$child->foreign_key],$urlAco);
					}
                }else{
                    $this->Acl->deny(['model' => 'Groups', "foreign_key"=>$id],$urlAco);
					foreach($childs as $skey => $child){
						$this->Acl->deny(['model' => 'Users', "foreign_key"=>$child->foreign_key],$urlAco);
					}
                }
            }
            $this->autoRender = false;
            $this->Flash->success(__('The group has been configured.'));
            return $this->redirect(['action' => 'index']);
        }
        $listAcos = $this->Acos->find('children', [
			'for' => 5,
			'contain'=>[
				'ParentAcos'
			]
		])->toArray();
		$this->loadModel('ArosAcos');
		$acos = [];
		foreach($listAcos as $key => $aco){
			$findAroAco = $this->ArosAcos->find('all',[
				'conditions' => [
					'aco_id' => $aco->id,
					'aro_id' => $group->aro->id,
					'_create' => 1
				]
			])->count();
			$isActive = false;
			if($findAroAco){
				$isActive = true;
			}
			$aco->is_active = $isActive;
			$acos[$key] = $aco;
		}
        $this->set('group', $group);
        $this->set('acos', $acos);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $group = $this->Groups->newEntity();
        if ($this->request->is('post')) {
            $group = $this->Groups->patchEntity($group, $this->request->data);
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('The group has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The group could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('group'));
        $this->set('_serialize', ['group']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $group = $this->Groups->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $group = $this->Groups->patchEntity($group, $this->request->data);
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('The group has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The group could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('group'));
        $this->set('_serialize', ['group']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $group = $this->Groups->get($id);
        if ($this->Groups->delete($group)) {
            $this->Flash->success(__('The group has been deleted.'));
        } else {
            $this->Flash->error(__('The group could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
