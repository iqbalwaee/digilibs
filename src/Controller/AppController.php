<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use AuditStash\Meta\RequestMetadata;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    protected $userId = null;
	protected $userData = null;
	
    
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        if($this->request->prefix == "stadmin"){
			$this->loadComponent('Datatables');
            $this->loadComponent('Acl.Acl' );
            $this->loadComponent('Auth', [
                'authorize' => [
                    'Acl.Actions' => ['actionPath' => 'controllers/']
                ],
                'authenticate' => [
                    'Form' => [
                        'passwordHasher' => [
                            'className' => 'St',
                        ]
                    ]
                ],
                'authError' => 'You are not authorized to access that location.',
                'flash' => [
                    'key' => 'flash',
                    'element' => 'error'
                ],
                'unauthorizedRedirect' => [
                    'controller' => 'Errors',
                    'action' => 'unauthorized',
                    'prefix' => 'stadmin'
                ],
            ]);
			$userAuth = $this->Auth->user();
			$this->userId = $userAuth['id'];
			$this->userData = $userAuth;
			$this->set('userAuth',$userAuth);
        }else{
             $this->loadComponent('Auth', [
                'authenticate' => [
                    'Form' => [
                        'userModel'=>'Members',
                        'passwordHasher' => [
                            'className' => 'St',
                        ]
                    ]
                ],
                'authError' => 'Please login first.',
                'flash' => [
                    'key' => 'flash',
                    'element' => 'error'
                ],
                 'loginAction' => [
                    'controller' => false,
                    'action' => 'login'
                ],
                'loginRedirect' => [
                    'controller' => false,
                    'action' => 'dashboard'
                ],
                'unauthorizedRedirect' => [
                    'controller' => 'Errors',
                    'action' => 'unauthorized',
                ],
                'storage' => ['className' => 'Session', 'key' => 'Auth.Member']
            ]);
            $this->Auth->allow(['display','login','register']);
            $this->loadModel('Settings');
            $data_settings = $this->Settings->find('all',['fields' => ['Settings.key','Settings.value']]);
            $settings = [];
            foreach ($data_settings as $key => $r) {
                $settings[$r->key]  = $r->value;
            }
            $this->webSettings = $settings;
            $session = $this->request->session();
            $carts    = $session->read('Cart');
            $countCart = count($carts);
            $this->set(compact('carts','countCart'));
            $this->set(compact('settings','carts','countCart'));    
            $userAuth = $this->Auth->user();
			$this->userId = $userAuth['id'];
			$this->userData = $userAuth;
			$this->set('userAuth',$userAuth);
        }
        
        
    }
    
	public function beforeFilter(Event $event)
    {
		if($this->request->prefix == "stadmin"){
			$eventManager = $this->loadModel()->eventManager();
			$eventManager->on(new RequestMetadata($this->request, $this->Auth->user('id')));
		}
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if($this->request->prefix == 'stadmin' && !$this->request->is('ajax')){
            $this->viewBuilder()->layout('stadmin_layout');
        }
        if($this->request->prefix == 'stadmin' && $this->request->action == 'login'){
            $this->viewBuilder()->layout('stadmin_login');
        }
        
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
}
