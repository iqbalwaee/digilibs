<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;

class CheckoutController extends AppController
{
    
    public function index()
    {
        $this->loadModel('PaymentMethods');
        $paymentMethods = $this->PaymentMethods->find('list');
        $this->set(compact('paymentMethods'));
        $session = $this->request->session();
        $carts    = $session->read('Cart');
        $countCart = count($carts);
        if($countCart == 0){
            return $this->redirect(['controller'=>false,'action'=>'my-cart']);
        }
        $totalAmount = 0;
        foreach($carts as $key => $catalog){
            if($this->userData['is_colleger'] == true){
                $totalAmount += 0;
            }else{
                $price = ($catalog->type == 1 ? $this->webSettings['Price.Thesis'] : $this->webSettings['Price.PracticalWork']);
                $totalAmount += $price;
            }
        }
        $this->set(compact('totalAmount'));
    }
    
    public function checkoutFinish()
    {
        $this->autoRender = false;
        $this->loadModel('Orders');
        $session = $this->request->session();
        $carts    = $session->read('Cart');
        $countCart = count($carts);
        if($countCart != 0){
            $totalAmount = 0;
            $dataDetail = [];
            foreach($carts as $key => $catalog){
                if($this->userData['is_colleger'] == true){
                    $price = 0;
                    $totalAmount += 0;
                }else{
                    $price = ($catalog->type == 1 ? $this->webSettings['Price.Thesis'] : $this->webSettings['Price.PracticalWork']);
                    $totalAmount += $price;
                }
                $dataDetail[] = [
                    'catalog_id' => $key,
                    'price' => $price
                ];
            }
            $payment_method_id = 0;
            $status = 1;
            if($totalAmount != 0){
                $payment_method_id = $this->request->data['payment_method_id'];
                $status = 0;
            }

            $order = $this->Orders->newEntity();
            $data  = [
                'member_id'  => $this->userData['id'],
                'total_amount' => $totalAmount,
                'payment_method_id' => $payment_method_id,
                'no_order' => rand(1000,99999),
                'unique_id' => Text::uuid(),
                'status' => $status,
                'status_payment'=>$status,
                'is_colleger' => $status,
                'order_details' => $dataDetail
            ];
            $order = $this->Orders->patchEntity($order,$data);
            if($this->Orders->save($order)){
                if($status == 1){
                    $errMsg = 'Your order has been approved. Please check your email to download your file.';
                }else{
                    $errMsg = 'Your order has been send, Please check your email to see your invoice.';
                }
                $this->response->body(json_encode(['errCode'=>0,'errMsg'=>$errMsg]));
            }else{
                $this->response->body(json_encode(['errCode'=>50,'errMsg'=>'Failed, Please try again ']));
            }    
        }else{
            $this->response->body(json_encode(['errCode'=>55,'errMsg'=>'Failed, Your cart is empty please fill your cart ']));
        }
        
    }

}

