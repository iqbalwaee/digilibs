<?php
namespace App\Controller;

use App\Controller\AppController;

class CatalogsController extends AppController
{
    public function listFile(){
        $conditions = [];
        $this->paginate = [
            'conditions' => $conditions,
            'limit' => 12,
            'order' => [
                'Catalogs.id' => 'asc'
            ],
            'contain' =>[
                'Members'
            ]
        ];
        $this->set('catalogs', $this->paginate($this->Catalogs));
    }
    
    public function index()
    {
        $conditions = ['Catalogs.member_id' => $this->userId];
        $this->paginate = [
            'conditions' => $conditions,
            'limit' => 12,
            'order' => [
                'Catalogs.id' => 'asc'
            ],
            'contain' =>[
                'Members'
            ]
        ];
        $this->set('catalogs', $this->paginate($this->Catalogs));
    }
    
    public function add()
    {
        $catalog = $this->Catalogs->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['member_id'] = $this->userId;
            $catalog = $this->Catalogs->patchEntity($catalog, $this->request->data);
            if ($this->Catalogs->save($catalog)) {
                $this->Flash->success(__('The file has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The file could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('catalog'));
        $this->set('_serialize', ['catalog']);
    }
    
    public function edit($id = null)
    {
        $catalog = $this->Catalogs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $catalog = $this->Catalogs->patchEntity($catalog, $this->request->data);
            if ($this->Catalogs->save($catalog)) {
                $this->Flash->success(__('The file has been updated.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The file could not be updated. Please, try again.'));
            }
        }
        $this->set(compact('catalog'));
        $this->set('_serialize', ['catalog']);
    }
    
    public function delete($id = null)
    {
        $this->request->allowMethod(['get', 'delete']);
        $catalog = $this->Catalogs->get($id);
        if($catalog->member_id == $this->userId){
            if ($this->Catalogs->delete($catalog)) {
                $this->Flash->success(__('The file has been deleted.'));
            } else {
                $this->Flash->error(__('The file could not be deleted. Please, try again.'));
            }
        }else{
            $this->Flash->error(__('The file could not be deleted. Your file is missing.'));
        }
        
        return $this->redirect(['action' => 'index']);
    }
}

