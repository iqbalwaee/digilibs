<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

class MembersController extends AppController
{
    public function index(){
        
    }
    
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
    
    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $time = new Time(date('Y-m-d H:i:s'));
                $user['last_login'] = $time;
                $member = $this->Members->get($user['id']);
                unset($user['password']);
                unset($member->password);
                $member = $this->Members->patchEntity($member, $user);
                $this->Members->save($member);
                return $this->redirect($this->Auth->redirectUrl());
            }else{
                $this->Flash->error(__('Invalid username or password, try again'));    
            }
        }
    }
    
    
    public function register()
    {
        $member = $this->Members->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['status'] = 1;
            $member = $this->Members->patchEntity($member, $this->request->data);
            if ($this->Members->save($member)) {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                }
                return $this->redirect(['controller'=>false,'action' => 'dashboard']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
		$jobs = $this->Members->Jobs->find('list');
        $this->set(compact('member','jobs'));
    }
}

