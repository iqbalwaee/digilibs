<?php
namespace App\Controller;

use App\Controller\AppController;

class LibrariesController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index','detail']);
    }

    public function index(){
        $this->loadModel('Catalogs');
         if($this->request->is('post')){
            if(!empty($this->request->data['tadvance'])){
                pr($this->request->data);
                return $this->redirect(['controller' => 'search', '?' =>[ 
                        'advance-search'=>true,
                        'type1' => $this->request->data['ttype1'],
                        'match1' => $this->request->data['tmacth1'],
                        'q1' => $this->request->data['tsearchfield1'],
                        'type2' => $this->request->data['ttype2'],
                        'match2' => $this->request->data['tmacth2'],
                        'q2' => $this->request->data['tsearchfield2'],
                        'type3' => $this->request->data['ttype3'],
                        'match3' => $this->request->data['tmacth3'],
                        'q3' => $this->request->data['tsearchfield3'],
                        'type4' => $this->request->data['ttype4'],
                        'match4' => $this->request->data['tmacth4'],
                        'q4' => $this->request->data['tsearchfield4']
                    ]
                ]);
            }else{
                $tsearch = $this->request->data['tsearch'];
            }
            return $this->redirect(['controller' => 'search', '?' =>[ 'q'=>$tsearch,
                        'advance-search'=>false ]]);
        }
        $conditions = [];
        $tquery = '';
        if(!empty($this->request->query['q']) && empty($this->request->query['advance-search'])){
            $query = $this->request->query['q'];
            $conditions += [
                'Members.display_name'.' LIKE' => '%'.$query.'%',
                'Catalogs.title'.' LIKE' => '%'.$query.'%',
                'Catalogs.subject'.' LIKE' => '%'.$query.'%',
            ];
            $tquery = $query;
        }elseif(!empty($this->request->query['advance-search'])){
            for($a = 1; $a <= 4 ; $a++){
                $query = $this->request->query['q'.$a];
                $type = $this->request->query['type'.$a];
                $match = $this->request->query['match'.$a];
                if(!empty($query)){
                    if($match == "MUST"){
                       if($type == 'any'){
                            $conditions = array_merge($conditions,[
                                'OR' => [
                                    'Members.display_name'.'' => $query,
                                    'Catalogs.title'.'' => $query,
                                    'Catalogs.subject'.'' => $query, 
                                ] 
                                    
                            ]);   
                       }else{
                          $conditions = array_merge($conditions,[
                                $type.'' => $query,
                        ]); 
                       }
                    }elseif($match == "PHRASE"){
                        if($type == 'any'){
                            $conditions = array_merge($conditions,[
                                'OR' => [
                                    'Members.display_name'.' LIKE' => $query,
                                    'Catalogs.title'.' LIKE' => $query,
                                    'Catalogs.subject'.' LIKE' => $query, 
                                ]

                            ]);   
                       }else{
                          $conditions = array_merge($conditions,[
                                    $type.' LIKE' => '%'.$query.'%',
                            ]); 
                       }
                    }elseif($match == "SHOULD"){
                        if($type == 'any'){
                            $conditions = array_merge($conditions,[
                                'OR' => [
                                    'Members.display_name'.' LIKE' => '%'.$query.'%',
                                    'Catalogs.title'.' LIKE' => '%'.$query.'%',
                                    'Catalogs.subject'.' LIKE' => '%'.$query.'%', 
                                ]

                            ]);   
                       }else{
                          $conditions = array_merge($conditions,[
                                    $type.' LIKE' => '%'.$query.'%',

                            ]); 
                       }
                    }elseif($match == "NOT"){
                        if($type == 'any'){
                            $conditions = array_merge($conditions,[
                                'OR' => [
                                    'Members.display_name'.' !=' => $query,
                                    'Catalogs.title'.' !=' => $query,
                                    'Catalogs.subject'.' !=' => $query, 
                                ]

                            ]);   
                       }else{
                            $conditions = array_merge($conditions,[
                                    $type.' !=' => $query,
                            ]); 
                       }
                    }   
                }
            }
        }
       // $this->autoRender = false;
        $this->paginate = [
            'conditions' => $conditions,
            'limit' => 2,
            'order' => [
                'Catalogs.id' => 'asc'
            ],
            'contain' =>[
                'Members'
            ]
        ];
        $this->set(compact('tquery'));
        $this->set('catalogs', $this->paginate($this->Catalogs));
    }
    
    public function detail()
    {
        $this->loadModel('Catalogs');
        $idCatalog = $this->request->params['id'];
        $catalog = $this->Catalogs->get($idCatalog,['contain' => ['Members']]);
        $this->set(compact('catalog'));
    }
    
}

