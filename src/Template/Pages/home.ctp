<div class="container-fluid">
    <div class="row">
        <div id="carousel-example-generic" class="main-carousel carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="<?=$this->request->base;?>/img/sliders/slide_1.jpg" alt="...">
                </div>
                <div class="item">
                    <img src="<?=$this->request->base;?>/img/sliders/slide_1.jpg" alt="...">
                </div>
            </div>
        </div>
    </div>
    <div class="form-search-home">
        <form action="<?=$this->request->base;?>/search" method="post" class="normal-search">
            <div class="form-group">
                <input type="hidden" name="tadvance" class="" placeholder="Search file" value="0">
                <input type="text" name="tsearch" class="" placeholder="Search file">
                <button type="submit"><i class="fa fa-search"></i></button>
            </div>
        </form>
        
        <form class="advance" action="<?=$this->request->base;?>/search" method="post">
            <input type="hidden" name="tadvance" class="" placeholder="Search file" value="1">
            <div class="row advanced-search">
                <div class="col-md-6">
                    <div class="form-group">
                        <select class="form-control width25" name="ttype1">
                            <option value="any">Any</option>
                            <option value="Members.display_name">Author</option>
                            <option value="Catalogs.title">Title</option>
                            <option value="Catalogs.title">Subject</option>
                        </select>
                        <select class="form-control width75" name="tmacth1">
                            <option value="MUST">has all of these words:</option>
                            <option value="PHRASE">has this exact phrase:</option>
                            <option value="SHOULD">has at least one of these words:</option>
                            <option value="NOT">has none of these words:</option>
                        </select>
                        <input type="text" name="tsearchfield1" class="" placeholder="Type keywords..">
                    </div>
                    <div class="form-group">
                        <select class="form-control width25" name="ttype2">
                            <option value="any">Any</option>
                            <option value="Members.display_name">Author</option>
                            <option value="Catalogs.title">Title</option>
                            <option value="Catalogs.title">Subject</option>
                        </select>
                        <select class="form-control width75" name="tmacth2">
                            <option value="MUST">has all of these words:</option>
                            <option value="PHRASE">has this exact phrase:</option>
                            <option value="SHOULD">has at least one of these words:</option>
                            <option value="NOT">has none of these words:</option>
                        </select>
                        <input type="text" name="tsearchfield2" class="" placeholder="Type keywords..">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <select class="form-control width25" name="ttype3">
                            <option value="any">Any</option>
                            <option value="Members.display_name">Author</option>
                            <option value="Catalogs.title">Title</option>
                            <option value="Catalogs.title">Subject</option>
                        </select>
                        <select class="form-control width75" name="tmacth3">
                            <option value="MUST">has all of these words:</option>
                            <option value="PHRASE">has this exact phrase:</option>
                            <option value="SHOULD">has at least one of these words:</option>
                            <option value="NOT">has none of these words:</option>
                        </select>
                        <input type="text" name="tsearchfield3" class="" placeholder="Type keywords..">
                    </div>
                    <div class="form-group">
                        <select class="form-control width25" name="ttype4">
                            <option value="any">Any</option>
                            <option value="Members.display_name">Author</option>
                            <option value="Catalogs.title">Title</option>
                            <option value="Catalogs.title">Subject</option>
                        </select>
                        <select class="form-control width75" name="tmacth4">
                            <option value="MUST">has all of these words:</option>
                            <option value="PHRASE">has this exact phrase:</option>
                            <option value="SHOULD">has at least one of these words:</option>
                            <option value="NOT">has none of these words:</option>
                        </select>
                        <input type="text" name="tsearchfield4" class="" placeholder="Type keywords..">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit">Search <i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="advanced-search">
                
                
            </div>
        </form>
        <div class="option-search">
            <div class="link-advance"><a href="#" class="link-advance">Advance search <i class="fa fa-search"></i></a></div>
            <div class="link-easy"><a href="#">Easy search <i class="fa fa-search"></i></a></div>
            <div class="link-file"><a href="<?=$this->request->base;?>/libraries">View all our library</a></div>
        </div>
    </div>
</div>