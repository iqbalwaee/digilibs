<?php
    $pluginCss = [
        'global/plugins/datatables/datatables.min.css',
        'global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
        'global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'
    ];
    $pluginScript = [
        'global/plugins/datatables/datatables.min.js',
        'global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
        'global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
        'pages/scripts/table-datatables-ajax.min.js',
    ];
?>
<?= $this->Html->css($pluginCss,['pathPrefix' => 'assets/backend/','block'=>'pluginCss']) ?>
<?= $this->Html->script($pluginScript,['pathPrefix' => 'assets/backend/','block'=>'pluginScript']) ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=$this->request->base;?>/stadmin/dashboard">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Catalogs</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php
                echo $this->Acl->link(
                    "<i class='fa fa-plus'></i> Add New Record",
                    [
                        'action' => 'add',
                    ],
                    [
                        'escape' => false,
                        'class' => 'btn green-jungle btn-sm btn-outline'
                    ]
                );
            ?>
        </div>
    </div>
</div>
<h3 class="page-title"> Catalogs Management
    <small>managing catalogs panel</small>
</h3>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dataTable order-column" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Actions</th>
                            <th>Name</th>
                            <th>Author</th>
                            <th>Status</th>
                            <th>Created</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var configTable = new Object();
    configTable = {
        urlDataTable : "<?=$this->request->here;?>",
        columnDefs : [{
            orderable: false, targets: [0,1],
            searchable : false, targets : [0,1],
        }],
        "columns": [
            { "name": "no" },
            { "name": "action" },
            { "name": "Catalogs.name" },
            { "name": "Members.username" },
            { "name": "Catalogs.status" },
            { "name": "Catalogs.created" },
          ]
    };
</script>