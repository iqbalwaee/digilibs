<?php
    $pluginCss = [
        'global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
    ];
    $pluginScript = [
        'global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
    ];
?>
<?= $this->Html->css($pluginCss,['pathPrefix' => 'assets/backend/','block'=>'pluginCss']) ?>
<?= $this->Html->script($pluginScript,['pathPrefix' => 'assets/backend/','block'=>'pluginScript']) ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=$this->request->base;?>/stadmin/dashboard">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <?php
            $backButton = false;
            if( $this->Acl->check(['action' => 'index'])){
                $backButton = true;
            }
            if($backButton){
                echo "<li>";
                $backButton = $this->Html->link(
                    "List Members Record",
                    [
                        'action' => 'index',
                    ],
                    [
                        'escape' => false,
                    ]
                );
                echo $backButton;
                echo "<i class='fa fa-circle'></i> </li>";
            }
        ?>
        <li>
            <span>Add New Member</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php
                if($backButton){
                    echo $this->Html->link(
                        "<i class='fa fa-list'></i> List Members Record",
                        [
                            'action' => 'index',
                        ],
                        [
                            'escape' => false,
                            'class' => 'btn green-jungle btn-sm btn-outline'
                        ]
                    );
                }
            ?>
        </div>
    </div>
</div>
<h3 class="page-title"> Members Management
    <small>managing members panel</small>
</h3>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-plus font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">Add New Member</span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">

                    </div>
                </div>
            </div>
            <div class="portlet-body form">
                <?= $this->Form->create($member,['class'=>'form-horizontal','type'=>'file']) ?>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Display Name</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('display_name', ['label'=>false,'class'=>'form-control input-sm']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Email</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('username', ['label'=>false,'class'=>'form-control input-sm']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Phone</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('phone', ['label'=>false,'class'=>'form-control input-sm']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Password</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('password', ['label'=>false,'class'=>'form-control input-sm']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Address</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('address', ['label'=>false,'class'=>'form-control input-sm']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">City</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('city', ['label'=>false,'class'=>'form-control input-sm']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Job</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('job_id', ['label'=>false,'class'=>'form-control input-sm','options'=>$jobs]);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Institution</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('institution', ['label'=>false,'class'=>'form-control input-sm']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Is Colleger</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('is_colleger', ['label'=>'Yes','class'=>'form-control input-sm']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Status</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('status', ['label'=>'Active','class'=>'form-control input-sm']);?>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                <?=$this->Form->end();?>
            </div>
        </div>
    </div>
</div>

