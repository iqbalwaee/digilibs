<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=$this->request->base;?>/stadmin/dashboard">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <?php
            $backButton = false;
            if( $this->Acl->check(['action' => 'index'])){
                $backButton = true;
            }
            if($backButton){
                echo "<li>";
                $backButton = $this->Html->link(
                    "List Jobs Record",
                    [
                        'action' => 'index',
                    ],
                    [
                        'escape' => false,
                    ]
                );
                echo $backButton;
                echo "<i class='fa fa-circle'></i> </li>";
            }
        ?>
        <li>
            <span>View Member</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php
                if($backButton){
                    echo $this->Html->link(
                        "<i class='fa fa-list'></i> List Jobs Record",
                        [
                            'action' => 'index',
                        ],
                        [
                            'escape' => false,
                            'class' => 'btn green-jungle btn-sm btn-outline'
                        ]
                    );
                }
            ?>
        </div>
    </div>
</div>
<h3 class="page-title"> Jobs Management
    <small>managing jobs panel</small>
</h3>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-magnifier font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">View Job</span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">

                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <dl class="dl-horizontal">
                   <dt><?=__('Display Name');?></dt>
                   <dd><?=$job->name;?></dd>
                   <dt><?=__('Status');?></dt>
                   <dd><?=$this->Utilities->statusActive($job->status);?></dd>
                   <dt><?=__('Created');?></dt>
                   <dd><?=$job->created;?></dd>
                   <dt><?=__('Modified');?></dt>
                   <dd><?=$job->modified;?></dd>
                </dl>
            </div>
        </div>
    </div>
</div>

