<?php
    $pluginCss = [
        'global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
    ];
    $pluginScript = [
        'global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
    ];
?>
<?= $this->Html->css($pluginCss,['pathPrefix' => 'assets/backend/','block'=>'pluginCss']) ?>
<?= $this->Html->script($pluginScript,['pathPrefix' => 'assets/backend/','block'=>'pluginScript']) ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=$this->request->base;?>/stadmin/dashboard">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <?php
            $backButton = false;
            if( $this->Acl->check(['action' => 'index'])){
                $backButton = true;
            }
            if($backButton){
                echo "<li>";
                $backButton = $this->Html->link(
                    "List Jobs Record",
                    [
                        'action' => 'index',
                    ],
                    [
                        'escape' => false,
                    ]
                );
                echo $backButton;
                echo "<i class='fa fa-circle'></i> </li>";
            }
        ?>
        <li>
            <span>Update Job</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php
                if($backButton){
                    echo $this->Html->link(
                        "<i class='fa fa-list'></i> List Jobs Record",
                        [
                            'action' => 'index',
                        ],
                        [
                            'escape' => false,
                            'class' => 'btn green-jungle btn-sm btn-outline'
                        ]
                    );
                }
            ?>
        </div>
    </div>
</div>
<h3 class="page-title"> Jobs Management
    <small>managing jobs panel</small>
</h3>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-pencil font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">Update Job</span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">

                    </div>
                </div>
            </div>
            <div class="portlet-body form">
				<?= $this->Form->create($job,['class'=>'form-horizontal','type'=>'file']) ?>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Name</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('name', ['label'=>false,'class'=>'form-control input-sm']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Status</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('status', ['label'=>'Active','class'=>'form-control input-sm']);?>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                <?=$this->Form->end();?>
            </div>
        </div>
    </div>
</div>

