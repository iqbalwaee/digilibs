<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=$this->request->base;?>/stadmin/dashboard">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <?php
            $backButton = false;
            if( $this->Acl->check(['action' => 'index'])){
                $backButton = true;
            }
            if($backButton){
                echo "<li>";
                $backButton = $this->Html->link(
                    "List Members Record",
                    [
                        'action' => 'index',
                    ],
                    [
                        'escape' => false,
                    ]
                );
                echo $backButton;
                echo "<i class='fa fa-circle'></i> </li>";
            }
        ?>
        <li>
            <span>View Member</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php
                if($backButton){
                    echo $this->Html->link(
                        "<i class='fa fa-list'></i> List Members Record",
                        [
                            'action' => 'index',
                        ],
                        [
                            'escape' => false,
                            'class' => 'btn green-jungle btn-sm btn-outline'
                        ]
                    );
                }
            ?>
        </div>
    </div>
</div>
<h3 class="page-title"> Members Management
    <small>managing members panel</small>
</h3>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-magnifier font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">View Member</span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">

                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <dl class="dl-horizontal">
                   <dt><?=__('Code');?></dt>
                   <dd><?=$member->code;?></dd>
                   <dt><?=__('Display Name');?></dt>
                   <dd><?=$member->display_name;?></dd>
                   <dt><?=__('Email');?></dt>
                   <dd><?=$member->username;?></dd>
                   <dt><?=__('Phone');?></dt>
                   <dd><?=$member->phone;?></dd>
                   <dt><?=__('Address');?></dt>
                   <dd><?=$member->address;?></dd>
                   <dt><?=__('City');?></dt>
                   <dd><?=$member->city;?></dd>
                   <dt><?=__('Job');?></dt>
                   <dd><?=$member->job->name;?></dd>
                   <dt><?=__('Institution');?></dt>
                   <dd><?=$member->institution;?></dd>
                   <dt><?=__('Is Colleger');?></dt>
                   <dd><?=$this->Utilities->statusYes($member->is_colleger);?></dd>
                   <dt><?=__('Status');?></dt>
                   <dd><?=$this->Utilities->statusActive($member->status);?></dd>
                   <dt><?=__('Created');?></dt>
                   <dd><?=$member->created;?></dd>
                   <dt><?=__('Modified');?></dt>
                   <dd><?=$member->modified;?></dd>
                </dl>
            </div>
        </div>
    </div>
</div>

