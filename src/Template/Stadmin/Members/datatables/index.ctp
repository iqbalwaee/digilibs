<?php
    $data = [];
    $start = $start + 1;
    foreach($result['data'] as $key => $val):
        $action = $this->Acl->startBlock();
        $action .= '<div class="btn-group"><button type="button" class="btn btn-xs green dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			        Actions&nbsp;<span class="caret"></span>
			    </button>
			    <ul class="dropdown-menu" role="menu">';
        $action .= $this->Acl->linklist(
            "<i class='fa fa-pencil'></i> Edit",
            [
                'action' => 'edit',
                $val->id
            ],
            [
                'escape' => false,
                'class' => ''
            ]
        );
        $action .= $this->Acl->linklist(
            "<i class='fa fa-search'></i> View",
            [
                'action' => 'view',
                $val->id
            ],
            [
                'escape' => false,
                'class' => ''
            ]
        );
        $action .= $this->Acl->linklist(
            "<i class='fa fa-trash-o'></i> Delete",
            [
                'action' => 'delete',
                $val->id
            ],
            [
                'escape' => false,
                'class' => 'btn-confirm-delete'
            ]
        );
        $action .= "</ul></div>";
        $action .= $this->Acl->endBlock();
        $data[] = [
            $start++,
            $action,
            $val->code,
            $val->display_name,
            $val->username,
            $val->phone,
            $val->created->format('Y/m/d H:i:s')
        ];
    endforeach;
    $json = $result;
    $json['data'] = $data;
    echo json_encode($json);
?>