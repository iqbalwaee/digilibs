<?php
    $data = [];
    $start = $start + 1;
    foreach($result['data'] as $key => $val):
        $action = $this->Acl->startBlock();
        $action = '<div class="text-center">';
        $action .= $this->Acl->link(
            "<i class='fa fa-pencil'></i>",
            [
                'action' => 'edit',
                $val->id
            ],
            [
                'escape' => false,
                'class' => 'btn btn-sm blue-steel'
            ]
        );
        $action .= "</div>";
        $action .= $this->Acl->endBlock();
        $data[] = [
            $start++,
            $val->title,
            $val->key,
            $val->value,
            $action
        ];
    endforeach;
    $json = $result;
    $json['data'] = $data;
    echo json_encode($json);
?>