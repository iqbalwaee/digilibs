<?php
    $pluginCss = [
        'global/plugins/datatables/datatables.min.css',
        'global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
        'global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'
    ];
    $pluginScript = [
        'global/plugins/datatables/datatables.min.js',
        'global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
        'global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
        'pages/scripts/table-datatables-ajax.min.js',
    ];
?>
<?= $this->Html->css($pluginCss,['pathPrefix' => 'assets/backend/','block'=>'pluginCss']) ?>
<?= $this->Html->script($pluginScript,['pathPrefix' => 'assets/backend/','block'=>'pluginScript']) ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=$this->request->base;?>/stadmin/dashboard">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Settings</span>
        </li>
    </ul>
</div>
<h3 class="page-title"> Settings
    <small>Managing setting panel</small>
</h3>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dataTable order-column" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Key</th>
                            <th>Value</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var configTable = new Object();
    configTable = {
        urlDataTable : "<?=$this->request->here;?>",
        columnDefs : [{
            orderable: false, targets: [0,-1],
            width: '140px', targets: '-1',
            searchable : false, targets : [0,-1],
        }],
        "columns": [
            { "name": "no" },
            { "name": "Settings.title" },
            { "name": "Settings.key" },
            { "name": "Settings.value" },
            { "name": "action" },
          ]
    };
</script>