<?php
    $data = [];
    $start = $start + 1;
    foreach($result['data'] as $key => $val):
        $action = $this->Acl->startBlock();
        $action = '<div class="text-center">';
        $action .= $this->Acl->link(
            "<i class='fa fa-pencil'></i>",
            [
                'action' => 'edit',
                $val->id
            ],
            [
                'escape' => false,
                'class' => 'btn btn-sm blue-steel'
            ]
        );
        $action .= $this->Acl->link(
            "<i class='fa fa-search'></i>",
            [
                'action' => 'view',
                $val->id
            ],
            [
                'escape' => false,
                'class' => 'btn btn-sm green-jungle'
            ]
        );
        $action .= $this->Acl->link(
            "<i class='fa fa-trash-o'></i>",
            [
                'action' => 'delete',
                $val->id
            ],
            [
                'escape' => false,
                'class' => 'btn btn-sm red-thunderbird btn-confirm-delete'
            ]
        );
        $action .= "</div>";
        $action .= $this->Acl->endBlock();
        $data[] = [
            $start++,
            ($val->parent_aco != null ? $val->parent_aco->alias : '' ),
            $val->alias,
            $action
        ];
    endforeach;
    $json = $result;
    $json['data'] = $data;
    echo json_encode($json);
?>