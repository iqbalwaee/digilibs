<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=$this->request->base;?>/stadmin/dashboard">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <?php
            $backButton = false;
            if( $this->Acl->check(['action' => 'index'])){
                $backButton = true;
            }
            if($backButton){
                echo "<li>";
                $backButton = $this->Html->link(
                    "List Acos Record",
                    [
                        'action' => 'index',
                    ],
                    [
                        'escape' => false,
                    ]
                );
                echo $backButton;
                echo "<i class='fa fa-circle'></i> </li>";
            }
        ?>
        <li>
            <span>View Aco</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php
                if($backButton){
                    echo $this->Html->link(
                        "<i class='fa fa-list'></i> List Acos Record",
                        [
                            'controller' => 'Acos',
                            'action' => 'index',
                        ],
                        [
                            'escape' => false,
                            'class' => 'btn green-jungle btn-sm btn-outline'
                        ]
                    );
                }
            ?>
        </div>
    </div>
</div>
<h3 class="page-title"> Acos Management
    <small>managing acos panel</small>
</h3>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-magnifier font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">View Aco</span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">

                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <dl class="dl-horizontal">
                    <dt><?= __('Parent Aco') ?></dt>
                    <dd><?= $aco->has('parent_aco') ? $this->Html->link($aco->parent_aco->alias, ['controller' => 'Acos', 'action' => 'view', $aco->parent_aco->id]) : '' ?></dd>
                    <dt><?= __('Model') ?></dt>
                    <dd><?= h($aco->model) ?></dd>
                    <dt><?= __('Alias') ?></dt>
                    <dd><?= h($aco->alias) ?></dd>
                    <dt><?= __('Foreign Key') ?></dt>
                    <dd><?= $this->Number->format($aco->foreign_key) ?></dd>
                </dl>
            </div>
        </div>
    </div>
</div>

