<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=$this->request->base;?>/stadmin/dashboard">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <?php
            $backButton = false;
            if( $this->Acl->check(['action' => 'index'])){
                $backButton = true;
            }
            if($backButton){
                echo "<li>";
                $backButton = $this->Html->link(
                    "List Users Record",
                    [
                        'action' => 'index',
                    ],
                    [
                        'escape' => false,
                    ]
                );
                echo $backButton;
                echo "<i class='fa fa-circle'></i> </li>";
            }
        ?>
        <li>
            <span>Update Group</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php
                if($backButton){
                    echo $this->Html->link(
                        "<i class='fa fa-list'></i> List Users Record",
                        [
                            'action' => 'index',
                        ],
                        [
                            'escape' => false,
                            'class' => 'btn green-jungle btn-sm btn-outline'
                        ]
                    );
                }
            ?>
        </div>
    </div>
</div>
<h3 class="page-title"> Users Management
    <small>managing users panel</small>
</h3>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">Configure Auth User <?=$user->username;?></span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">

                    </div>
                </div>
            </div>
            <div class="portlet-body form">
                <?= $this->Form->create('aros_acos',['class'=>'','type'=>'post']) ?>
                    <table class="table table-condensed table-striped table-hovered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th width="50px">Allow</th>
                                <th width="50px">Disallow</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($acos as $key => $aco):?>
								<?php 
									$activeAllow = false;
									$activeDisallow = false;
									if($aco->is_active){
										$activeAllow = true;
									}else{
										$activeDisallow = true;
									}
								?>
                                <tr>
                                    <td>
                                        <?= $this->Form->input('aros_acos['.$key.'][aco_id]',['value' => $aco->id,'type'=>'hidden','label'=>false]);?>
                                        <?=$aco->parent_aco->alias." <i class=\"fa fa-angle-right\"></i> ".$aco->alias;?>
                                    </td>
                                    <td>
                                        <?php
                                           echo $this->Form->radio(
                                                'aros_acos['.$key.'][allow]',
                                                [
                                                    ['value' => '1', 'text' => false,'checked'=>$activeAllow,'parent_id'=>$aco->parent_id,'class' => $aco->id],
                                                ],
                                                ['hiddenField'=>false]
                                            );
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            echo $this->Form->radio(
                                                'aros_acos['.$key.'][allow]',
                                                [
                                                    ['value' => '2', 'text' => false,'checked'=>$activeDisallow,'parent_id'=>$aco->parent_id,'class' => $aco->id],
                                                ],
                                                ['hiddenField'=>false]
                                            );
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                <?=$this->Form->end();?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->scriptBlock(
	'
		$("[type=\'radio\']").on("click",function(){
			console.log($(this).val());
			var thisVal = $(this).val();
			var thisClass = $(this).attr("class");
			var thisAllow = $("input[class=\""+thisClass+"\"][value=\"1\"]");
			var thisSpanAllow		= thisAllow.closest("span");
			var thisDisallow = $("input[class=\""+thisClass+"\"][value=\"0\"]");
			var thisSpanDisallow		= thisDisallow.closest("span");
			var thisParentId = $(this).attr("parent_id");
			var radioParentAllow 	= $("."+thisParentId+"[value=\"1\"]");
			var parentSpanAllow		= radioParentAllow.closest("span");
			var radioParentDisalow 	= $("."+thisParentId+"[value=\"0\"]");
			var parentSpanDisallow	= radioParentDisalow.closest("span");
			var childAllow 			= $("input[parent_id=\""+thisClass+"\"][value=\"1\"]");
			var childSpanAllow		= childAllow.closest("span");
			var childDisallow 		= $("input[parent_id=\""+thisClass+"\"][value=\"0\"]");
			var childSpanDisallow	= childDisallow.closest("span");
			if(thisVal == 1){
				thisDisallow.removeAttr("checked");
				thisSpanDisallow.removeClass("checked");
				thisAllow.attr("checked","checked");
				thisSpanAllow.addClass("checked");
				thisAllow.attr("checked","checked");
				radioParentAllow.attr("checked","checked");
				parentSpanAllow.addClass("checked")
				radioParentDisalow.removeAttr("checked");
				parentSpanDisallow.removeClass("checked");
				childAllow.attr("checked","checked");
				childSpanAllow.addClass("checked")
				childDisallow.removeAttr("checked");
				childSpanDisallow.removeClass("checked");
			}else{
				thisAllow.removeAttr("checked");
				thisSpanAllow.removeClass("checked");
				thisDisallow.attr("checked","checked");
				thisSpanDisallow.addClass("checked");
				childDisallow.attr("checked","checked");
				childSpanDisallow.addClass("checked")
				childAllow.removeAttr("checked");
				childSpanAllow.removeClass("checked");
				var sameParentAllow		= $("input[parent_id=\""+thisParentId+"\"][value=\"1\"][checked=\"checked\"]");
				if(sameParentAllow.length == 0){
					radioParentAllow.removeAttr("checked");
					parentSpanAllow.removeClass("checked")
					radioParentDisalow.attr("checked","checked");
					parentSpanDisallow.addClass("checked");
				}
			}
		})
	'
,['block'=>'script']);

