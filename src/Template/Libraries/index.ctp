<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            
        </div>
        <div class="col-md-9">
            <div class="box">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->Flash->render();?>
                            <h1 class="box-title">
                                <?=(empty($tquery) ? __('Showing All Files') : __('Search File By : ').$tquery);?>
                            </h1>
                            <?php
                                echo $this->Paginator->counter('Page {{page}} of {{pages}}, showing {{current}} records out of {{count}} total' );
                            ?>
                        </div>
                        <?php foreach($catalogs as $catalog):?>
                            <div class="col-md-3">
                                <?php $niceUrl = $this->Utilities->inflectorHumanize($catalog->title);?>
                                <a href="<?=$this->request->base;?>/libraries/<?=$niceUrl;?>/<?=$catalog->id;?>" class="catalog">
                                    <?php $image = $this->Utilities->generateUrlImage('catalogs/img/'.$catalog->img_dir,$catalog->img,'square_');?>
                                    <div class="catalog-image">
                                        <img src="<?=$image;?>" alt="<?=$catalog->title;?>">
                                    </div>
                                    <div class="catalog-information">
                                        <p class="title"><?=$catalog->title;?></p>
                                        <p class="subject"><?=$catalog->subject;?></p>
                                        <p class="post-date"><i class="fa fa-calendar"></i> <?=$catalog->created;?></p>
                                    </div>
                                    <div class="catalog-author">
                                        <p class="author"><i class="fa fa-user"></i> <?=$catalog->member->display_name;?></p>
                                        <p class="viewed"><i class="fa fa-eye"></i> <?=$catalog->viewed;?></p>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pagination-custom">
                                <ul class="pagination">
                                    <?php
                                        echo $this->Paginator->prev('<i class="fa fa-chevron-left"></i>', array('escape'=>false,'tag' => 'li'), null, array('escape'=>false, 'tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                                        echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                                        echo $this->Paginator->next('<i class="fa fa-chevron-right"></i>', array('escape'=>false, 'tag' => 'li','currentClass' => 'disabled'), null, array('escape'=>false,'tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>