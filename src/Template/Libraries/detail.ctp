<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            
        </div>
        <div class="col-md-9">
            <div class="box">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->Flash->render();?>
                            <h1 class="box-sub-title">
                                <?=$catalog->title;?>
                            </h1>
                            <div class="catalog-detail">
                                <div class="image-cover">
                                    <?php $image = $this->Utilities->generateUrlImage('catalogs/img/'.$catalog->img_dir,$catalog->img,'square_');?>
                                    <img src="<?=$image;?>">
                                </div>
                                <div class="catalog-information">
                                    <dl class="dl-horizontal">
                                        <dt>Type File</dt>
                                        <dd><?=($catalog->type == 1 ? 'Thesis' : 'Practical Work' );?></dd>
                                        <dt>Language</dt>
                                        <dd><?=($catalog->language == 0 ? 'English' : 'Indonesia' );?></dd>
                                        <dt>Identifier</dt>
                                        <dd><?=$catalog->identifier;?></dd>
                                        <dt>Subject</dt>
                                        <dd><?=$catalog->subject;?></dd>
                                        <dt>Alt. Subject</dt>
                                        <dd><?=$catalog->alt_subject;?></dd>
                                        <dt>Keyword</dt>
                                        <dd>
                                            <ul class="tags">
                                                <?php
                                                    $exKeyword = explode(',',$catalog->keyword);
                                                    foreach($exKeyword as $keyword):
                                                        echo "<li><span>".$keyword."</span></li>";
                                                    endforeach;
                                                ?>
                                            </ul>
                                        </dd>
                                        <dt>Writter</dt>
                                        <dd><?=$catalog->writter;?></dd>
                                        <dt>Authors</dt>
                                        <dd><?=$catalog->member->display_name;?></dd>
                                        <dt>Contributor</dt>
                                        <dd>
                                            <ul class="tags">
                                                <?php
                                                    $exContributor = explode(',',$catalog->contributor);
                                                    foreach($exContributor as $contributor):
                                                        echo "<li><span>".$contributor."</span></li>";
                                                    endforeach;
                                                ?>
                                            </ul>
                                        </dd>
                                        <?php if($userAuth == null || $userAuth['is_colleger'] == false):?>
                                            <dt>Price For Download</dt>
                                            <dd><span class="price">IDR<?=($catalog->type == 1 ? $this->Number->precision($settings['Price.Thesis'],0) : $this->Number->precision($settings['Price.PracticalWork'],0));?></span></dd>
                                        <?php else:?>
                                            <dt>Price For Download</dt>
                                            <dd><span class="price">FREE</span></dd>
                                        <?php endif;?>
                                        <dt>Posted Date</dt>
                                        <dd><?=$catalog->created;?></dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="catalog-action">
                                <a href="#" class="btn btn-primary pull-right addToCart" data-catalog-id="<?=$catalog->id;?>"><i class="fa fa-shopping-cart"></i> Add To Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>