<?php
    $cakeDescription = 'Admin Template ST';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?= $this->Html->charset() ?>
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('favicon.png', 'img/favicon.png',['type'=>'icon']);?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Sundanese Technology" name="author" />
    
    <?php
        $externalCss = [
            'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all',
            'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
			'https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.2.4/css/simple-line-icons.min.css'
        ];
        $mandatoryCss = [
            'global/plugins/bootstrap/css/bootstrap.min.css',
            'global/plugins/uniform/css/uniform.default.css',
            'global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
            'global/css/bootstrap-timepicker.min.css',
            'global/css/components.min.css',
            'global/css/plugins.min.css',
            'global/css/bootstrap-fileinput.css'
            
        ];
        $themeCss = [
            'layouts/layout/css/layout.min.css',
            'layouts/layout/css/profile.min.css',
            'layouts/layout/css/themes/darkblue.min.css',
            'layouts/layout/css/custom.min.css'
        ];
    ?>
    <?= $this->Html->css($externalCss,['block'=>'externalCss']) ?>
    <?= $this->Html->css($mandatoryCss,['pathPrefix' => 'assets/backend/','block'=>'mandatoryCss']) ?>
    <?= $this->Html->css($themeCss,['pathPrefix' => 'assets/backend/','block'=>'themeCss']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('externalCss') ?>
    <?= $this->fetch('mandatoryCss') ?>
    <?= $this->fetch('pluginCss') ?>
    <?= $this->fetch('themeCss') ?>
</head>
<body class="page-sidebar-closed-hide-logo page-content-white page-header-fixed page-sidebar-fixed page-footer-fixed">
    <?= $this->element('backend/header');?>
    <?= $this->element('backend/container');?>
    <?= $this->element('backend/footer');?>
    <?php
        $coreScript = [
            'global/plugins/jquery.min.js',
            'global/plugins/bootstrap/js/bootstrap.min.js',
            'global/plugins/js.cookie.min.js',
            'global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
            'global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
            'global/plugins/jquery.blockui.min.js',
            'global/plugins/uniform/jquery.uniform.min.js',
            'global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        ];
        $themeScript = [
            'global/scripts/bootstrap-timepicker.min.js',
            'global/scripts/app.min.js',
            'global/scripts/custom-app.min.js',
            'global/scripts/bootstrap-fileinput.js',
            'layouts/layout/scripts/layout.min.js',
            'layouts/global/scripts/quick-sidebar.min.js',
            'global/scripts/jquery.counterup.min.js',
            'global/scripts/jquery.waypoints.min.js',
            'pages/scripts/components-date-time-pickers.min.js',
            'global/plugins/flot/jquery.flot.min.js',
            'global/plugins/flot/jquery.flot.resize.min.js',
            'global/plugins/flot/jquery.flot.categories.min.js',
            'global/scripts/app.min.js',
            'global/scripts/dashboard.min.js'
        ]
    ?>
    <?= $this->Html->script($coreScript,['pathPrefix' => 'assets/backend/','block'=>'coreScript']) ?>
    <?= $this->Html->script($themeScript,['pathPrefix' => 'assets/backend/','block'=>'themeScript']) ?>
    <?= $this->fetch('externalScript') ?>
    <?= $this->fetch('coreScript') ?>
    <?= $this->fetch('pluginScript') ?>
    <?= $this->fetch('themeScript') ?>
    <?= $this->fetch('script') ?>
</body>
</html>
