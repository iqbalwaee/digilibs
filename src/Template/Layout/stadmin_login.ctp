<?php
    $cakeDescription = 'Login Admin Template ST';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?= $this->Html->charset() ?>
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('favicon.png', 'img/favicon.png',['type'=>'icon']);?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Sundanese Technology" name="author" />
    
    <?php
        $externalCss = [
            'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all',
            'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'
        ];
        $mandatoryCss = [
            'global/plugins/simple-line-icons/simple-line-icons.min.css',
            'global/plugins/bootstrap/css/bootstrap.min.css',
            'global/plugins/uniform/css/uniform.default.css',
            'global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
            'global/css/components.min.css',
            'global/css/plugins.min.css',
            'global/plugins/select2/css/select2.min.css',
            'global/plugins/select2/css/select2-bootstrap.min.css',
            
            
        ];
        $themeCss = [
            'pages/css/login-5.min.css',
        ];
    ?>
    <?= $this->Html->css($externalCss,['block'=>'externalCss']) ?>
    <?= $this->Html->css($mandatoryCss,['pathPrefix' => 'assets/backend/','block'=>'mandatoryCss']) ?>
    <?= $this->Html->css($themeCss,['pathPrefix' => 'assets/backend/','block'=>'themeCss']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('externalCss') ?>
    <?= $this->fetch('mandatoryCss') ?>
    <?= $this->fetch('pluginCss') ?>
    <?= $this->fetch('themeCss') ?>
</head>
<body  class="login">
    <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-6 login-container bs-reset">
                    <div class="login-content">
                        <h1>Admin Panel Login</h1>
                        <p> Halaman ini hanya bisa diakses oleh admin pengurus website. Segala tindak hacking / cracking akan diproses sesuai jalur hukum yang berlaku. </p>
                        <form action="<?=$this->request->here;?>" class="login-form" method="post">
                            <?= $this->Flash->render('flash') ?>
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Enter any username and password. </span>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <?php 
                                      echo $this->Form->input('username',
                                        [
                                            'placeholder' => 'Username',
                                            'label' => false,
                                            'type' => 'text',
                                            'autocomplete' => 'off',
                                            'class' => 'form-control form-control-solid placeholder-no-fix form-group',
                                            'required' => true
                                        ]
                                      );
                                    ?>
                                </div>
                                <div class="col-xs-6">
                                    <?php 
                                      echo $this->Form->input('password',
                                        [
                                            'placeholder' => 'Password',
                                            'label' => false,
                                            'type' => 'password',
                                            'autocomplete' => 'off',
                                            'class' => 'form-control form-control-solid placeholder-no-fix form-group',
                                            'required' => true
                                        ]
                                      );
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="rem-password">
                                        <p>Remember Me
                                            <input type="checkbox" class="rem-checkbox" />
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-8 text-right">
                                    <button class="btn blue" type="submit">Sign In</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div class="col-xs-5 bs-reset">
                            </div>
                            <div class="col-xs-7 bs-reset">
                                <div class="login-copyright text-right">
                                    <p>Copyright &copy; Sundanese Technology</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 bs-reset">
                    <div class="login-bg"> </div>
                </div>
            </div>
        </div>
    <?php
        $coreScript = [
            'global/plugins/jquery.min.js',
            'global/plugins/bootstrap/js/bootstrap.min.js',
            'global/plugins/js.cookie.min.js',
            'global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
            'global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
            'global/plugins/jquery.blockui.min.js',
            'global/plugins/uniform/jquery.uniform.min.js',
            'global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
            'global/plugins/jquery-validation/js/jquery.validate.min.js',
            'global/plugins/jquery-validation/js/additional-methods.min.js',
            'global/plugins/select2/js/select2.full.min.js',
            'global/plugins/backstretch/jquery.backstretch.min.js'
        ];
        $themeScript = [
            'global/scripts/app.min.js',
            'global/scripts/custom-app.min.js',
            'pages/scripts/login-5.min.js'
        ]
    ?>
    <?= $this->Html->script($coreScript,['pathPrefix' => 'assets/backend/','block'=>'coreScript']) ?>
    <?= $this->Html->script($themeScript,['pathPrefix' => 'assets/backend/','block'=>'themeScript']) ?>
    <?= $this->fetch('externalScript') ?>
    <?= $this->fetch('coreScript') ?>
    <?= $this->fetch('pluginScript') ?>
    <?= $this->fetch('themeScript') ?>
    <?= $this->fetch('script') ?>
</body>
</html>
