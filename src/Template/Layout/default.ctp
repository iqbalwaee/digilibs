<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta name="author" content="Sundanese Technology" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />
		<title>
		<?= $this->fetch('title'); ?> | 
        <?= $settings['Site.title'];?>
        <?= (!empty($settings['Site.tagline']) ? ' | ' . $settings['Site.tagline'] : '' );?>
    	</title>
    	<?= $this->Html->meta('favicon.png', 'img/favicon.png',['type'=>'icon']);?>
		<?php
		$themeCss = [
            'digilibs/digilibs.css'
        ];
    	?>
	<?= $this->Html->css($themeCss,['pathPrefix' => 'assets/frontend/','block'=>'themeCss']) ?>
    <?= $this->fetch('meta');?>
	<?= $this->fetch('themeCss') ?>
	<?= $this->fetch('mandatoryCss') ?>
	
	
	</head>
	<body id="body">
        <?= $this->element('frontend/main-template');?>
		<?php
		$coreScript = [
			'digilibs/digilibs.js',
		];
		$pluginScript = [
			
		];
	?>
	<?= $this->Html->script($coreScript,['pathPrefix' => 'assets/frontend/','block'=>'coreScript']) ?>
	<?= $this->Html->script($pluginScript,['pathPrefix' => 'assets/frontend/','block'=>'pluginScript']) ?>
    <?= $this->fetch('coreScript') ?>
    <?= $this->fetch('pluginScript') ?>
    <?= $this->fetch('script') ?>
    <script>
        var baseUrl = "<?=$this->request->base;?>";    
    </script>
	</body>
</html>