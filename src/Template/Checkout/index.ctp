<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="box-title">Checkout</h1>
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                Summary
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <dl class="dl-horizontal">
                                            <dt>Total Amount</dt>
                                            <dd><span class="price"><?=($totalAmount != 0 ? $this->Number->precision($totalAmount,0) : 'Free');?></span></dd>
                                            <dt>Total File</dt>
                                            <dd><?=$countCart;?> File<?($countCart > 1 ? 's' : '');?></dd>
                                        </dl>
                                    </div>
                                    </div>
                                </div>
                                <?php if($totalAmount != 0):?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingTwo">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                                    Payment Method
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                            <div class="panel-body">
                                                <?php foreach($paymentMethods as $key => $val):?>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="payment_method_id" class="payment_method_id" value="<?=$key;?>  ">
                                                            <?=$val;?>
                                                        </label>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif;?>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      List Cart
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-cart">
                                            <thead>
                                                <tr>
                                                    <th>Cover</th>
                                                    <th>File Name</th>
                                                    <th>Type</th>
                                                    <th>Author</th>
                                                    <th>Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($carts as $key => $catalog):?>
                                                    <tr>
                                                        <td width="100px">
                                                            <?php $image = $this->Utilities->generateUrlImage('catalogs/img/'.$catalog->img_dir,$catalog->img,'square_');?>
                                                            <img src="<?=$image;?>" class="image-cover">
                                                        </td>
                                                        <td>
                                                            <?php $niceUrl = $this->Utilities->inflectorHumanize($catalog->title);?>
                                                            <a href="<?=$this->request->base;?>/libraries/<?=$niceUrl;?>/<?=$catalog->id;?>">
                                                                <?=$catalog->title;?>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <?=$catalog->member->display_name;?>
                                                        </td>
                                                        <td>
                                                            <?=($catalog->type == 1 ? 'Thesis' : 'Practical Work' );?>
                                                        </td>
                                                        <td>
                                                            <?php if($userAuth == null || $userAuth['is_colleger'] == false):?>
                                                                <span class="price">IDR<?=($catalog->type == 1 ? $this->Number->precision($settings['Price.Thesis'],0) : $this->Number->precision($settings['Price.PracticalWork'],0));?></span>
                                                            <?php else:?>
                                                                <span class="price">FREE</span>
                                                            <?php endif;?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <a href="<?=$this->request->base;?>/my-cart" class="btn btn-default pull-left"><i class="fa fa-shopping-cart"></i> Edit Cart</a>
                            <a href="#" class="btn btn-primary pull-right finishCheckout"><i class="fa fa-check-circle"></i> Finish</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>