<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="box-title">My Cart
                            <div class="box-action">
                                <?php if($countCart != 0):?>
                                <a href="<?=$this->request->base;?>/checkout" class="btn btn-default">Checkout <i class="fa fa-arrow-right"></i></a>    
                                <?php endif;?>
                            </div>
                            </h1>
                            <?php if($countCart != 0):?>
                            <div class="table-responsive">
                                <table class="table table-cart">
                                    <thead>
                                        <tr>
                                            <th>Cover</th>
                                            <th>File Name</th>
                                            <th>Author</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($carts as $key => $catalog):?>
                                            <tr>
                                                <td width="100px">
                                                    <?php $image = $this->Utilities->generateUrlImage('catalogs/img/'.$catalog->img_dir,$catalog->img,'square_');?>
                                                    <img src="<?=$image;?>" class="image-cover">
                                                </td>
                                                <td>
                                                    <?php $niceUrl = $this->Utilities->inflectorHumanize($catalog->title);?>
                                                    <a href="<?=$this->request->base;?>/libraries/<?=$niceUrl;?>/<?=$catalog->id;?>">
                                                        <?=$catalog->title;?>
                                                    </a><br>
                                                    <a href="#" data-catalog-id="<?=$catalog->id;?>" class="removeFromCart">[<i class="fa fa-times"></i> Remove]</a>
                                                </td>
                                                <td>
                                                    <?=$catalog->member->display_name;?>
                                                </td>
                                                <td>
                                                    <?php if($userAuth == null || $userAuth['is_colleger'] == false):?>
                                                        <span class="price">IDR<?=($catalog->type == 1 ? $this->Number->precision($settings['Price.Thesis'],0) : $this->Number->precision($settings['Price.PracticalWork'],0));?></span>
                                                    <?php else:?>
                                                        <span class="price">FREE</span>
                                                    <?php endif;?>
                                                </td>
                                            </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <?php else:?>
                                <div class="alert alert-danger"><b>Your cart is empty</b></div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>