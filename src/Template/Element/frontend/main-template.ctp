<header>
    <nav class="navbar-top">
        <div class="welcome-message">
            <?=__('Welcome to digital library');?>
        </div>
        <div class="social-media">
            <ul>
                <li><a href="#" class="info">info@digilibs.com</a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
            </ul>
        </div>
    </nav>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=$this->request->base;?>/"><img src="<?=$this->request->base;?>/img/DIGILIBS.png" alt="digilibs logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?=$this->request->base;?>/libraries"><i class="fa fa-briefcase"></i> Libraries</a></li>
                    <?php if(empty($userAuth)):?>
                        <li><a href="<?=$this->request->base;?>/login"><i class="fa fa-unlock"></i> LOGIN</a></li>
                        <li><a href="<?=$this->request->base;?>/register"><i class="fa fa-user"></i> REGISTER</a></li>
                    <li><a href="<?=$this->request->base;?>/my-cart"><i class="fa fa-shopping-cart"></i> Cart <span class="badge badge-cart"><?=$countCart;?></span></a></li>
                    <?php else:?>
                        <li><a href="<?=$this->request->base;?>/dashboard"><i class="fa fa-user"></i> Member Area</a></li>
                    <li><a href="<?=$this->request->base;?>/my-cart"><i class="fa fa-shopping-cart"></i> Cart <span class="badge badge-cart"><?=$countCart;?></span></a></li>
                        <li><a href="<?=$this->request->base;?>/logout"><i class="fa fa-lock"></i> Logout</a></li>
                    <?php endif;?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>
<section class="wrapper-content">
    <?=$this->fetch('content');?>
</section>