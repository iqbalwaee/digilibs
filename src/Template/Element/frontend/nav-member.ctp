<ul class="nav nav-tabs">
  <li role="presentation" class="<?=($active == 'dashboard' ? 'active' : '');?>"><a href="<?=$this->request->base;?>/dashboard">Dashboard</a></li>
  <li role="presentation" class="<?=($active == 'catalogs' ? 'active' : '');?>"><a href="<?=$this->request->base;?>/my-files">My Files</a></li>
  <li role="presentation" class="<?=($active == 'payment-confirmation' ? 'active' : '');?>"><a href="<?=$this->request->base;?>/payment-confirmation">Payment Confirmation</a></li>
</ul>