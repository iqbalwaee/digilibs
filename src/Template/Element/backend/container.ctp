<div class="page-container">
    <?= $this->element('backend/sidebar');?>
    <div class="page-content-wrapper">
        <div class="page-content">
			<?= $this->Flash->render();?>
            <?= $this->fetch('content');?>
        </div>
    </div>
</div>
<div id="confirmMessage" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p> Are you sure want to delete this record? </p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-outline dark">No</button>
                <button type="button"  class="btn green btn-yes">Yes</button>
            </div>
        </div>
    </div>
</div>