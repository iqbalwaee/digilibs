<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler"> </div>
            </li>
            <li class="nav-item start">
                <a href="<?=$this->request->base;?>/stadmin/dashboard" class="nav-link">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            
            <?php $this->Acl->startBlock() ?>
                <li class="heading">
                    <h3 class="uppercase">Featured</h3>
                </li>
                
                <li>
                    <?= $this->Acl->link(
                        __('<i class="icon-user"></i><span class="title"> Members</span>'),
                        [
                            'controller' => 'Members',
                            'action' => 'index',
                            'prefix' => 'stadmin'
                        ],
                        ['escape' => false]
                    ) ?>
                </li>
                <li>
                    <?= $this->Acl->link(
                        __('<i class="icon-user"></i><span class="title"> Jobs</span>'),
                        [
                            'controller' => 'Jobs',
                            'action' => 'index',
                            'prefix' => 'stadmin'
                        ],
                        ['escape' => false]
                    ) ?>
                </li>
                <li>
                    <?= $this->Acl->link(
                        __('<i class="icon-config"></i><span class="title"> Settings</span>'),
                        [
                            'controller' => 'Settings',
                            'action' => 'index',
                            'prefix' => 'stadmin'
                        ],
                        ['escape' => false]
                    ) ?>
                </li>
                <li class="heading">
                    <h3 class="uppercase">System Setting</h3>
                </li>
                <li>
                    <?= $this->Acl->link(
                        __('<i class="icon-docs"></i><span class="title"> Groups Management</span>'),
                        [
                            'controller' => 'Groups',
                            'action' => 'index',
                            'prefix' => 'stadmin'
                        ],
                        ['escape' => false]
                    ) ?>
                </li>
                <li>
                    <?= $this->Acl->link(
                        __('<i class="icon-people"></i><span class="title"> Users Management</span>'),
                        [
                            'controller' => 'Users',
                            'action' => 'index',
                            'prefix' => 'stadmin'
                        ],
                        ['escape' => false]
                    ) ?>
                </li>
                <li>
                    <?= $this->Acl->link(
                        __('<i class="icon-drawer"></i><span class="title"> Acos Management</span>'),
                        [
                            'controller' => 'Acos',
                            'action' => 'index',
                            'prefix' => 'stadmin'
                        ],
                        ['escape' => false]
                    ) ?>
                </li>
            <?php $this->Acl->endBlock() ?>
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>