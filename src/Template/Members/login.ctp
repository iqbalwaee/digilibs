<section class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <h2 class="box-title">Login</h2>
                <div class="box-body">
                    <?= $this->Flash->render();?>
                    <?=$this->Form->create();?>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                                <?=$this->Form->input('username',['class'=>'form-control','label'=>false,'placeholder'=>'Email','div'=>false,'type'=>'email']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock"></i></span>
                                <?=$this->Form->input('password',['class'=>'form-control','label'=>false,'placeholder'=>'Password']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="pull-left">
                                <button class="btn btn-primary" type="submit">LOGIN</button>
                            </div>
                            <div class="pull-right">
                                <a href="<?=$this->request->base;?>">Forgot password</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    <?=$this->Form->end();?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <h2 class="box-title">No have account?</h2>
                <div class="box-body">
                    <p>Click link below to create your digilibs account.<br><a href="<?=$this->request->base;?>/register">Register Now</a></p>
                </div>
            </div>
        </div>
    </div>
</section>