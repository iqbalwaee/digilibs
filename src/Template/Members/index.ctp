<section class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-lg-3">
            <?=$this->Element('frontend/profile-box');?>
        </div>
        <div class="col-md-8 col-lg-9">
            <?=$this->Element('frontend/nav-member',['active'=>'dashboard']);?>
            <div class="box-tab">
                <?=$this->Flash->render();?>
            </div>
        </div>
    </div>
</section>