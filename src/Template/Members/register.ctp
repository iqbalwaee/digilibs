<section class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
            <div class="box">
                <h2 class="box-title text-center">Register</h2>
                <div class="box-body">
                    <?=$this->Form->create($member);?>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
                                <?=$this->Form->input('display_name',['class'=>'form-control','label'=>false,'placeholder'=>'Name','div'=>false]);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                                <?=$this->Form->input('username',['class'=>'form-control','label'=>false,'placeholder'=>'Email','div'=>false,'type'=>'email']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-phone"></i></span>
                                <?=$this->Form->input('phone',['class'=>'form-control','label'=>false,'placeholder'=>'Phone','div'=>false]);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock"></i></span>
                                <?=$this->Form->input('password',['class'=>'form-control','label'=>false,'placeholder'=>'Password']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-briefcase"></i></span>
                                <?=$this->Form->input('job_id',['class'=>'form-control','label'=>false,'empty'=>'Choose your job','options'=>$jobs]);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-building"></i></span>
                                <?=$this->Form->input('institution',['class'=>'form-control','label'=>false,'placeholder'=>'Institution','div'=>false]);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit">REGISTER</button>
                            <div class="clearfix"></div>
                            <div class="text-center">
                                <a href="<?=$this->request->base;?>/login">Already have account</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    <?=$this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
</section>