<section class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-lg-3">
            <?=$this->Element('frontend/profile-box');?>
        </div>
        <div class="col-md-8 col-lg-9">
            <?=$this->Element('frontend/nav-member',['active'=>'catalogs']);?>
            <div class="box-tab">
                <?=$this->Form->create($catalog,['type'=>'file']);?>
                    <h1 class="title-box">Update File<div class="pull-right"><a class="btn btn-default" href="<?=$this->request->base;?>/my-files"><i class="fa fa-arrow-left"></i> LIST MY FILES</a> <a class="btn btn-danger" href="<?=$this->request->base;?>/delete-my-files/<?=$catalog->id;?>"><i class="fa fa-trash-o"></i> DELETE FILE</a>  <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> UPDATE</button></div><div class="clearfix"></div></h1>
                    <?=$this->Flash->render();?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 col-md-3 control-label">Title</label>
                                    <div class="col-sm-8 col-md-9">
                                        <?=$this->Form->input('title',['class'=>'form-control','label'=>false,'placeholder'=>'Title file...']);?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 col-md-3 control-label">Subject</label>
                                    <div class="col-sm-8 col-md-9">
                                        <?=$this->Form->input('subject',['class'=>'form-control','label'=>false,'placeholder'=>'Subject file...']);?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 col-md-3 control-label">Alt Subject</label>
                                    <div class="col-sm-8 col-md-9">
                                        <?=$this->Form->input('alt_subject',['class'=>'form-control','label'=>false,'placeholder'=>'Alternative subject...']);?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 col-md-3 control-label">Keyword</label>
                                    <div class="col-sm-8 col-md-9">
                                        <?=$this->Form->input('keyword',['class'=>'form-control','label'=>false,'placeholder'=>'Keyword file...']);?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 col-md-3 control-label">Language</label>
                                    <div class="col-sm-8 col-md-9">
                                        <?=$this->Form->input('language',['class'=>'form-control','label'=>false,'empty'=>'Choose language...','options'=>['English','Indonesia']]);?>
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 col-md-3 control-label">Type File</label>
                                    <div class="col-sm-8 col-md-9">
                                        <?=$this->Form->input('type',['class'=>'form-control','label'=>false,'empty'=>'Choose type file','options' => [1=>'Thesis',2 =>'Practical Work']]);?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 col-md-3 control-label">Cover File</label>
                                    <div class="col-sm-8 col-md-9">
                                        <?=$this->Form->input('img',['class'=>'btn btn-default','label'=>false,'type'=>'file']);?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 col-md-3 control-label">Document File</label>
                                    <div class="col-sm-8 col-md-9">
                                        <?=$this->Form->input('file',['class'=>'btn btn-default','label'=>false,'type'=>'file']);?>
                                        <span class="help-block">Only PDF,DOC and DOCX File</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 col-md-3 control-label">Writter</label>
                                    <div class="col-sm-8 col-md-9">
                                        <?=$this->Form->input('writter',['class'=>'form-control','label'=>false,'placeholder'=>'Writter...']);?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 col-md-3 control-label">Contributor</label>
                                    <div class="col-sm-8 col-md-9">
                                        <?=$this->Form->input('contributor',['class'=>'form-control','label'=>false,'placeholder'=>'Contributor...']);?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 col-md-3 control-label">Identifier</label>
                                    <div class="col-sm-8 col-md-9">
                                        <?=$this->Form->input('identifier',['class'=>'form-control','label'=>false,'placeholder'=>'Identifier...']);?>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                <?=$this->Form->end();?>
            </div>
        </div>
    </div>
</section>