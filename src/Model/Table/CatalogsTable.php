<?php
namespace App\Model\Table;

use App\Model\Entity\Catalog;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use PhpOffice\PhpWord\IOFactory;
use Asika\Pdf2text;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;

/**
 * Catalogs Model
 */
class CatalogsTable extends Table
{

    
    public $tmpDirFile = '';
    public $strFile = '';
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('catalogs');
        $this->displayField('title');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Members', [
            'foreignKey' => 'member_id',
            'joinType' => 'INNER'
        ]);
        $this->addBehavior('Proffer.Proffer', [
            'img' => [    // The name of your upload field
                'root' => WWW_ROOT . 'resources', // Customise the root upload folder here, or omit to use the default
                'dir' => 'img_dir',   // The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    'square' => [   // Define the prefix of your thumbnail
                        'w' => 200, // Width
                        'h' => 200, // Height
                        'crop' => true,  // Crop will crop the image as well as resize it
                        'jpeg_quality'  => 100,
                        'png_compression_level' => 9
                    ],'show' => [   // Define the prefix of your thumbnail
                        'w' => 400, // Width
                        'h' => 400, // Height
                        'crop' => true,  // Crop will crop the image as well as resize it
                        'jpeg_quality'  => 100,
                        'png_compression_level' => 9
                    ],
                ]
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->add('type', 'valid', ['rule' => 'numeric'])
            ->requirePresence('type', 'create')
            ->notEmpty('type');
            
        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');
        
        $validator
            ->allowEmpty('img');
        
        $validator
            ->requirePresence('file', 'create')
            ->add('file','custom',[
                'rule' => [$this,'checkFileUpload'],
                'message' => 'Extension is not valid'
            ])
            ->notEmpty('file')
            ->allowEmpty('file','update');
            
        $validator
            ->add('status', 'valid', ['rule' => 'numeric']);
            
        $validator
            ->requirePresence('writter', 'create')
            ->notEmpty('writter');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['member_id'], 'Members'));
        return $rules;
    }
    
    public function checkFileUpload($value,$contex){
        
        $errorMsg = '';
        if(!empty($_FILES['file'])){
            $errors= array();
            $file_name  =   $_FILES['file']['name'];
            $file_size  =   $_FILES['file']['size'];
            $file_tmp   =   $_FILES['file']['tmp_name'];
            $file_type  =   $_FILES['file']['type'];
            $exp_file   =   explode('.',$file_name);
            $file_ext   =   strtolower(end($exp_file));
            $validations_ext = ['docx','doc','pdf'];
            
            if(in_array($file_ext,$validations_ext)){
                $tmpDir = TMP.$file_name;
                $this->tmpDirFile = $tmpDir;
                move_uploaded_file($file_tmp,$tmpDir);
                $file = new File($tmpDir, true, 0777);
                if($contex['data']['type'] == 1){
                    if($file_ext == "pdf"){
                        $reader = new Pdf2text;
                        $reader->setFilename($tmpDir);
                        $reader->decodePDF();
                        $output = $reader->output();
                        $str = $output;
                    }else{
                        $phpWord  = IOFactory::load($tmpDir);
                        $objWriter = IOFactory::createWriter($phpWord, 'HTML'); 
                        $file_name = rand(0,9000);
                        $savedFile = TMP.$file_name.'.txt';
                        $objWriter->save($savedFile);
                        $content = file_get_contents($savedFile);
                        $strExplode = explode("<style>",$content);
                        $strExplode2 = explode("8pt;}",$strExplode[1]);
                        $str = strip_tags($strExplode2[2]);
                        $file = new File($savedFile);
                        $file->delete();
                    }
                    if(strpos(strtolower($str),'abstract') == true || strpos(strtolower($str),'abstrak') == true ){
                        $this->strFile = $str;
                        return true;
                    }else{
                        return "Not found abcstract on this file";
                    }
                }
                
            }else{
                return false;
            }
        }
        return true;
    }
    
    public function beforeSave($event, $entity, $options){
        if(!empty($_FILES['file'])){
            if($entity->isNew() == false){
                $folder = new Folder(WWW_ROOT.$entity->file_dir);
                $folder->delete();
            }
            $entity->file =  $_FILES['file']['name'];
            $rand = rand(0,90000);
            $fileDir = WWW_ROOT.'resources'.DS.'catalogs'.DS.'files'.DS.$rand.DS;
            $entity->file_dir =  'resources'.DS.'catalogs'.DS.'files'.DS.$rand.DS;
            $fileDes = $fileDir.$_FILES['file']['name'];
            $folder = new Folder();
            if ($folder->create($fileDir,0755)) {
                $file = new File($this->tmpDirFile);
                $file->copy($fileDes,true);
                $file->delete();
            }
        }
	}
}
