<?php
namespace App\Model\Table;

use App\Model\Entity\Member;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Auth\StPasswordHasher;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Groups
 */
class MembersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('members');
        $this->displayField('id');
        $this->primaryKey('id');
		$this->addBehavior('AuditStash.AuditLog');
        $this->addBehavior('Timestamp');
        
		$this->belongsTo('Jobs',[
			'foreignKey' => 'job_id'
		]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username','Please fill email field')
            ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table','message'=>'This email already exist']);
		
        $validator
            ->requirePresence('phone', 'create')
            ->notEmpty('phone','Please fill phone field');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password','Please fill password field','create')
            ->allowEmpty('password','update');
		
		$validator
            ->requirePresence('job_id', 'create')
            ->notEmpty('job_id','Please choose job field');
		
		$validator
            ->requirePresence('institution', 'create')
            ->notEmpty('institution','Please fill institution field');
		
		$validator
            ->add('is_colleger', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('is_colleger');
		
		$validator
            ->add('status', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('status');

        return $validator;
    }
	
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        
        return $rules;
    }
    
	public function beforeSave($event, $entity, $options){
		if(!empty($entity->password)){
			$abstract = new StPasswordHasher(); 
			$entity->password = StPasswordHasher::hash($entity->password);
		}
		
		if($entity->isNew()){
			$characters = strtoupper('abcdefghijklmnopqrstuvwxyz0123456789');
			$string = '';
			for ($i = 0; $i < 7; $i++) {
			  $string .= $characters[rand(0, strlen($characters) - 1)];
			}
			$entity->code = $string;
		}
		
	}
	
}
