<?php
namespace App\Model\Table;

use App\Model\Entity\Aro;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Aros Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Groups
 */
class ArosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('Aros');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Acl.Acl', ['type' => 'requester']);

        $this->hasOne('Users', [
            'foreignKey' => 'foreign_key',
            'conditions' => ['model' => 'Users'],
            'joinType' => 'INNER'
        ]);
		
		$this->hasOne('Groups', [
            'foreignKey' => 'foreign_key',
            'conditions' => ['model' => 'Groups'],
            'joinType' => 'INNER'
        ]);
		
		
    }
    
}
