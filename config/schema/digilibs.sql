-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 09, 2016 at 08:28 
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digilibs`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_phinxlog`
--

CREATE TABLE `acl_phinxlog` (
  `version` bigint(20) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_phinxlog`
--

INSERT INTO `acl_phinxlog` (`version`, `start_time`, `end_time`) VALUES
(20141229162641, '2016-04-10 19:19:14', '2016-04-10 19:19:15');

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE `acos` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, NULL, NULL, 'controllers', 1, 124),
(5, 1, NULL, NULL, 'Stadmin', 2, 97),
(6, 5, NULL, NULL, 'Acos', 3, 14),
(7, 6, NULL, NULL, 'index', 4, 5),
(8, 6, NULL, NULL, 'view', 6, 7),
(9, 6, NULL, NULL, 'add', 8, 9),
(10, 6, NULL, NULL, 'edit', 10, 11),
(11, 6, NULL, NULL, 'delete', 12, 13),
(13, 5, NULL, NULL, 'Dashboard', 15, 18),
(14, 13, NULL, NULL, 'index', 16, 17),
(16, 5, NULL, NULL, 'Groups', 19, 32),
(17, 16, NULL, NULL, 'index', 20, 21),
(18, 16, NULL, NULL, 'view', 22, 23),
(19, 16, NULL, NULL, 'configure', 24, 25),
(20, 16, NULL, NULL, 'add', 26, 27),
(21, 16, NULL, NULL, 'edit', 28, 29),
(22, 16, NULL, NULL, 'delete', 30, 31),
(24, 5, NULL, NULL, 'Pages', 33, 36),
(25, 24, NULL, NULL, 'display', 34, 35),
(27, 5, NULL, NULL, 'Users', 37, 54),
(28, 27, NULL, NULL, 'index', 38, 39),
(31, 27, NULL, NULL, 'view', 40, 41),
(32, 27, NULL, NULL, 'add', 42, 43),
(33, 27, NULL, NULL, 'edit', 44, 45),
(34, 27, NULL, NULL, 'delete', 46, 47),
(47, 1, NULL, NULL, 'Pages', 98, 101),
(48, 47, NULL, NULL, 'display', 99, 100),
(49, 27, NULL, NULL, 'login', 48, 49),
(50, 27, NULL, NULL, 'logout', 50, 51),
(51, 1, NULL, NULL, 'Acl', 102, 103),
(52, 1, NULL, NULL, 'Bake', 104, 105),
(53, 1, NULL, NULL, 'DebugKit', 106, 121),
(54, 53, NULL, NULL, 'Panels', 107, 112),
(55, 54, NULL, NULL, 'index', 108, 109),
(56, 54, NULL, NULL, 'view', 110, 111),
(57, 53, NULL, NULL, 'Requests', 113, 116),
(58, 57, NULL, NULL, 'view', 114, 115),
(59, 53, NULL, NULL, 'Toolbar', 117, 120),
(60, 59, NULL, NULL, 'clearCache', 118, 119),
(61, 1, NULL, NULL, 'Migrations', 122, 123),
(62, 27, '', NULL, 'configure', 52, 53),
(63, 5, '', NULL, 'Members', 55, 68),
(64, 63, '', NULL, 'index', 56, 57),
(65, 63, '', NULL, 'add', 58, 59),
(66, 63, '', NULL, 'edit', 60, 61),
(67, 63, '', NULL, 'delete', 62, 63),
(68, 63, '', NULL, 'view', 64, 65),
(69, 63, NULL, NULL, 'configure', 66, 67),
(88, 5, '', NULL, 'Jobs', 69, 80),
(89, 88, '', NULL, 'index', 70, 71),
(90, 88, '', NULL, 'add', 72, 73),
(91, 88, '', NULL, 'edit', 74, 75),
(92, 88, '', NULL, 'delete', 76, 77),
(93, 5, '', NULL, 'Catalogs', 81, 90),
(94, 93, '', NULL, 'index', 82, 83),
(95, 93, '', NULL, 'add', 84, 85),
(96, 93, '', NULL, 'delete', 86, 87),
(97, 93, '', NULL, 'view', 88, 89),
(98, 88, '', NULL, 'view', 78, 79),
(99, 5, '', NULL, 'Settings', 91, 96),
(100, 99, '', NULL, 'index', 92, 93),
(101, 99, '', NULL, 'edit', 94, 95);

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

CREATE TABLE `aros` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Groups', 1, NULL, 1, 6),
(2, NULL, 'Groups', 2, NULL, 7, 8),
(8, 1, 'Users', 1, NULL, 2, 3),
(9, 1, 'Users', 2, NULL, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

CREATE TABLE `aros_acos` (
  `id` int(11) NOT NULL,
  `aro_id` int(11) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 1, 1, '-1', '-1', '-1', '-1'),
(2, 1, 6, '1', '1', '1', '1'),
(3, 8, 6, '1', '1', '1', '1'),
(4, 9, 6, '1', '1', '1', '1'),
(5, 1, 7, '1', '1', '1', '1'),
(6, 8, 7, '1', '1', '1', '1'),
(7, 9, 7, '1', '1', '1', '1'),
(8, 1, 8, '1', '1', '1', '1'),
(9, 8, 8, '1', '1', '1', '1'),
(10, 9, 8, '1', '1', '1', '1'),
(11, 1, 9, '1', '1', '1', '1'),
(12, 8, 9, '1', '1', '1', '1'),
(13, 9, 9, '1', '1', '1', '1'),
(14, 1, 10, '1', '1', '1', '1'),
(15, 8, 10, '1', '1', '1', '1'),
(16, 9, 10, '1', '1', '1', '1'),
(17, 1, 11, '1', '1', '1', '1'),
(18, 8, 11, '1', '1', '1', '1'),
(19, 9, 11, '1', '1', '1', '1'),
(20, 1, 13, '1', '1', '1', '1'),
(21, 8, 13, '1', '1', '1', '1'),
(22, 9, 13, '1', '1', '1', '1'),
(23, 1, 14, '1', '1', '1', '1'),
(24, 8, 14, '1', '1', '1', '1'),
(25, 9, 14, '1', '1', '1', '1'),
(26, 1, 16, '1', '1', '1', '1'),
(27, 8, 16, '1', '1', '1', '1'),
(28, 9, 16, '1', '1', '1', '1'),
(29, 1, 17, '1', '1', '1', '1'),
(30, 8, 17, '1', '1', '1', '1'),
(31, 9, 17, '1', '1', '1', '1'),
(32, 1, 18, '1', '1', '1', '1'),
(33, 8, 18, '1', '1', '1', '1'),
(34, 9, 18, '1', '1', '1', '1'),
(35, 1, 19, '1', '1', '1', '1'),
(36, 8, 19, '1', '1', '1', '1'),
(37, 9, 19, '1', '1', '1', '1'),
(38, 1, 20, '1', '1', '1', '1'),
(39, 8, 20, '1', '1', '1', '1'),
(40, 9, 20, '1', '1', '1', '1'),
(41, 1, 21, '1', '1', '1', '1'),
(42, 8, 21, '1', '1', '1', '1'),
(43, 9, 21, '1', '1', '1', '1'),
(44, 1, 22, '1', '1', '1', '1'),
(45, 8, 22, '1', '1', '1', '1'),
(46, 9, 22, '1', '1', '1', '1'),
(47, 1, 24, '1', '1', '1', '1'),
(48, 8, 24, '1', '1', '1', '1'),
(49, 9, 24, '1', '1', '1', '1'),
(50, 1, 25, '1', '1', '1', '1'),
(51, 8, 25, '1', '1', '1', '1'),
(52, 9, 25, '1', '1', '1', '1'),
(53, 1, 27, '1', '1', '1', '1'),
(54, 8, 27, '1', '1', '1', '1'),
(55, 9, 27, '1', '1', '1', '1'),
(56, 1, 28, '1', '1', '1', '1'),
(57, 8, 28, '1', '1', '1', '1'),
(58, 9, 28, '1', '1', '1', '1'),
(59, 1, 31, '1', '1', '1', '1'),
(60, 8, 31, '1', '1', '1', '1'),
(61, 9, 31, '1', '1', '1', '1'),
(62, 1, 32, '1', '1', '1', '1'),
(63, 8, 32, '1', '1', '1', '1'),
(64, 9, 32, '1', '1', '1', '1'),
(65, 1, 33, '1', '1', '1', '1'),
(66, 8, 33, '1', '1', '1', '1'),
(67, 9, 33, '1', '1', '1', '1'),
(68, 1, 34, '1', '1', '1', '1'),
(69, 8, 34, '1', '1', '1', '1'),
(70, 9, 34, '1', '1', '1', '1'),
(71, 1, 49, '1', '1', '1', '1'),
(72, 8, 49, '1', '1', '1', '1'),
(73, 9, 49, '1', '1', '1', '1'),
(74, 1, 50, '1', '1', '1', '1'),
(75, 8, 50, '1', '1', '1', '1'),
(76, 9, 50, '1', '1', '1', '1'),
(77, 1, 62, '1', '1', '1', '1'),
(78, 8, 62, '1', '1', '1', '1'),
(79, 9, 62, '1', '1', '1', '1'),
(80, 1, 63, '1', '1', '1', '1'),
(81, 8, 63, '1', '1', '1', '1'),
(82, 9, 63, '1', '1', '1', '1'),
(83, 1, 64, '1', '1', '1', '1'),
(84, 8, 64, '1', '1', '1', '1'),
(85, 9, 64, '1', '1', '1', '1'),
(86, 1, 65, '1', '1', '1', '1'),
(87, 8, 65, '1', '1', '1', '1'),
(88, 9, 65, '1', '1', '1', '1'),
(89, 1, 66, '1', '1', '1', '1'),
(90, 8, 66, '1', '1', '1', '1'),
(91, 9, 66, '1', '1', '1', '1'),
(92, 1, 67, '1', '1', '1', '1'),
(93, 8, 67, '1', '1', '1', '1'),
(94, 9, 67, '1', '1', '1', '1'),
(95, 1, 68, '1', '1', '1', '1'),
(96, 8, 68, '1', '1', '1', '1'),
(97, 9, 68, '1', '1', '1', '1'),
(98, 1, 69, '1', '1', '1', '1'),
(99, 8, 69, '1', '1', '1', '1'),
(100, 9, 69, '1', '1', '1', '1'),
(101, 1, 88, '1', '1', '1', '1'),
(102, 8, 88, '1', '1', '1', '1'),
(103, 9, 88, '1', '1', '1', '1'),
(104, 1, 89, '1', '1', '1', '1'),
(105, 8, 89, '1', '1', '1', '1'),
(106, 9, 89, '1', '1', '1', '1'),
(107, 1, 90, '1', '1', '1', '1'),
(108, 8, 90, '1', '1', '1', '1'),
(109, 9, 90, '1', '1', '1', '1'),
(110, 1, 91, '1', '1', '1', '1'),
(111, 8, 91, '1', '1', '1', '1'),
(112, 9, 91, '1', '1', '1', '1'),
(113, 1, 92, '1', '1', '1', '1'),
(114, 8, 92, '1', '1', '1', '1'),
(115, 9, 92, '1', '1', '1', '1'),
(116, 1, 93, '1', '1', '1', '1'),
(117, 8, 93, '1', '1', '1', '1'),
(118, 9, 93, '1', '1', '1', '1'),
(119, 1, 94, '1', '1', '1', '1'),
(120, 8, 94, '1', '1', '1', '1'),
(121, 9, 94, '1', '1', '1', '1'),
(122, 1, 95, '1', '1', '1', '1'),
(123, 8, 95, '1', '1', '1', '1'),
(124, 9, 95, '1', '1', '1', '1'),
(125, 1, 96, '1', '1', '1', '1'),
(126, 8, 96, '1', '1', '1', '1'),
(127, 9, 96, '1', '1', '1', '1'),
(128, 1, 97, '1', '1', '1', '1'),
(129, 8, 97, '1', '1', '1', '1'),
(130, 9, 97, '1', '1', '1', '1'),
(131, 8, 1, '-1', '-1', '-1', '-1'),
(132, 1, 98, '-1', '-1', '-1', '-1'),
(133, 8, 98, '-1', '-1', '-1', '-1'),
(134, 9, 98, '-1', '-1', '-1', '-1'),
(135, 1, 99, '1', '1', '1', '1'),
(136, 8, 99, '1', '1', '1', '1'),
(137, 9, 99, '1', '1', '1', '1'),
(138, 1, 100, '1', '1', '1', '1'),
(139, 8, 100, '1', '1', '1', '1'),
(140, 9, 100, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `audits`
--

CREATE TABLE `audits` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `transaction` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `primary_key` varchar(255) DEFAULT NULL,
  `source` text,
  `parent_source` text,
  `source_id` text,
  `changed` text,
  `meta` text,
  `ip_address` text NOT NULL,
  `browser_info` text NOT NULL,
  `pc_name` text NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audits`
--

INSERT INTO `audits` (`id`, `timestamp`, `transaction`, `type`, `primary_key`, `source`, `parent_source`, `source_id`, `changed`, `meta`, `ip_address`, `browser_info`, `pc_name`, `user_id`) VALUES
(1, '2016-05-23 10:31:39', '22ac59cf-b835-44bd-9575-7e5163585de8', 'create', '3', 'jobs', NULL, NULL, '{"id":3,"name":"asdsadsa","status":false}', '{"ip":"127.0.0.1","url":"\\/digilibs\\/stadmin\\/jobs\\/add","user":1}', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0', 'lutfi-X450JB', 0),
(2, '2016-05-23 10:32:15', '989472b7-3232-4955-b61f-7233c6cecce6', 'delete', '3', 'members', NULL, NULL, NULL, '{"ip":"127.0.0.1","url":"\\/digilibs\\/stadmin\\/members\\/delete\\/3","user":1}', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0', 'lutfi-X450JB', 0),
(3, '2016-05-24 06:14:49', '044a1c14-f740-4292-8f42-51e36805cc43', 'update', '1', 'members', NULL, NULL, '{"password":"$2y$10$MNDMthKPWz3qb0l\\/YrOyK.pjE7IE\\/o49Jk9fN7rTTUI0W1uI0RNBC"}', '{"ip":"127.0.0.1","url":"\\/digilibs\\/stadmin\\/members\\/edit\\/1","user":1}', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0', 'lutfi-X450JB', 0),
(4, '2016-05-24 06:15:18', 'e7ce4b3e-0373-4ce4-a4d4-166c8e9f8ea5', 'update', '1', 'members', NULL, NULL, '{"password":"$2y$10$oJ4Xavt6AsGzvIBP7C2qMeSQsnQHAmkZkYUre5K8\\/\\/9UtCIK36D3y"}', '{"ip":"127.0.0.1","url":"\\/digilibs\\/stadmin\\/members\\/edit\\/1","user":1}', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0', 'lutfi-X450JB', 0),
(5, '2016-05-24 06:20:51', '59d95315-3155-41af-b621-608ab541c041', 'update', '1', 'members', NULL, NULL, '{"password":"9eb3123fbdcdad2d3722be3ae0610e9f06690ed5"}', '{"ip":"127.0.0.1","url":"\\/digilibs\\/stadmin\\/members\\/edit\\/1","user":1}', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0', 'lutfi-X450JB', 0),
(6, '2016-05-24 06:39:22', '49120add-1a6b-4f4e-8b82-7ce8f9cba9fb', 'update', '1', 'members', NULL, NULL, '{"password":"9eb3123fbdcdad2d3722be3ae0610e9f06690ed5"}', '{"ip":"127.0.0.1","url":"\\/digilibs\\/stadmin\\/members\\/edit\\/1","user":1}', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0', 'lutfi-X450JB', 0),
(7, '2016-05-24 06:40:03', '63816291-ee63-4e56-b6d6-4c97f63c726f', 'update', '1', 'members', NULL, NULL, '{"password":"9eb3123fbdcdad2d3722be3ae0610e9f06690ed5"}', '{"ip":"127.0.0.1","url":"\\/digilibs\\/stadmin\\/members\\/edit\\/1","user":1}', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0', 'lutfi-X450JB', 0),
(8, '2016-05-24 06:40:57', '0ade3724-e937-4cc9-a619-128a96409d45', 'update', '1', 'members', NULL, NULL, '{"password":"9eb3123fbdcdad2d3722be3ae0610e9f06690ed5"}', '{"ip":"127.0.0.1","url":"\\/digilibs\\/stadmin\\/members\\/edit\\/1","user":1}', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0', 'lutfi-X450JB', 0);

-- --------------------------------------------------------

--
-- Table structure for table `audit_log_phinxlog`
--

CREATE TABLE `audit_log_phinxlog` (
  `version` bigint(20) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audit_log_phinxlog`
--

INSERT INTO `audit_log_phinxlog` (`version`, `start_time`, `end_time`) VALUES
(20151210084102, '2016-04-19 02:26:15', '2016-04-19 02:26:16'),
(20151210085037, '2016-04-19 02:26:16', '2016-04-19 02:26:16');

-- --------------------------------------------------------

--
-- Table structure for table `catalogs`
--

CREATE TABLE `catalogs` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `type` tinyint(2) NOT NULL,
  `title` varchar(225) NOT NULL,
  `img` varchar(225) NOT NULL,
  `img_dir` varchar(225) NOT NULL,
  `file` text NOT NULL,
  `file_dir` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `writter` varchar(225) NOT NULL,
  `contributor` text NOT NULL,
  `subject` varchar(225) NOT NULL,
  `alt_subject` text NOT NULL,
  `keyword` text NOT NULL,
  `identifier` varchar(225) NOT NULL,
  `language` varchar(225) NOT NULL,
  `abstract` text NOT NULL,
  `viewed` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `catalogs`
--

INSERT INTO `catalogs` (`id`, `member_id`, `type`, `title`, `img`, `img_dir`, `file`, `file_dir`, `status`, `writter`, `contributor`, `subject`, `alt_subject`, `keyword`, `identifier`, `language`, `abstract`, `viewed`, `created`, `modified`) VALUES
(15, 1, 1, 'IMPLEMENTASI ALGORITMA CHAIN CODE DAN LVQ PADA PENGENALAN HURUF HIJAIYAH', 'clockwork.jpg', 'd2458d14-2b18-43b9-bd28-cd7085575ca7', 'Isi Laporan TA.pdf', 'resources/catalogs/files/61608/', 0, 'Udin', 'Udin, Sumarno, Sutejo', 'PENGENALAN HURUF HIJAIYAH', 'PENGENALAN HURUF HIJAIYAH', 'PENGENALAN HURUF HIJAIYAH, IMPLEMENTASI ALGORITMA CHAIN CODE DAN LVQ', 'dsadsad', '1', '', 0, '2016-06-06 18:39:23', '2016-06-10 07:45:07');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Administrator', '2016-04-11 02:40:32', '2016-04-11 02:40:32'),
(2, 'Users', '2016-04-11 02:41:26', '2016-04-11 02:41:26');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `name` char(64) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `status`, `created`, `modified`) VALUES
(3, 'asdsadsa', 0, '2016-05-23 10:31:39', '2016-05-23 10:31:39');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `code` char(10) NOT NULL,
  `display_name` varchar(64) DEFAULT NULL,
  `username` varchar(32) NOT NULL,
  `password` text NOT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `address` text NOT NULL,
  `city` char(225) NOT NULL,
  `job_id` int(11) NOT NULL,
  `institution` varchar(225) NOT NULL,
  `is_colleger` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `code`, `display_name`, `username`, `password`, `phone`, `address`, `city`, `job_id`, `institution`, `is_colleger`, `last_login`, `created`, `modified`, `status`) VALUES
(1, 'GB0TDBW', 'Iqbal', 'iqbal.sundanese1@gmail.com', '9eb3123fbdcdad2d3722be3ae0610e9f06690ed5', '08568046517', '', '', 3, 'wakwaw', 1, '2016-06-29 08:23:24', '2016-05-24 06:10:45', '2016-06-29 08:23:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `no_order` int(11) NOT NULL,
  `unique_id` varchar(225) NOT NULL,
  `member_id` int(11) NOT NULL,
  `total_amount` decimal(11,2) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `status_payment` tinyint(1) NOT NULL,
  `is_colleger` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `no_order`, `unique_id`, `member_id`, `total_amount`, `payment_method_id`, `status`, `status_payment`, `is_colleger`, `created`, `modified`) VALUES
(1, 0, '', 1, '0.00', 0, 1, 1, 1, '2016-06-29 08:28:31', '2016-06-29 08:28:31'),
(2, 0, '', 1, '0.00', 0, 1, 1, 1, '2016-06-29 08:44:04', '2016-06-29 08:44:04'),
(3, 0, '', 1, '0.00', 0, 1, 1, 1, '2016-06-29 08:44:52', '2016-06-29 08:44:52'),
(4, 0, '', 1, '0.00', 0, 1, 1, 1, '2016-06-29 08:54:01', '2016-06-29 08:54:01'),
(5, 0, '', 1, '0.00', 0, 1, 1, 1, '2016-06-29 08:55:37', '2016-06-29 08:55:37'),
(6, 0, '', 1, '0.00', 0, 1, 1, 1, '2016-06-29 08:55:41', '2016-06-29 08:55:41'),
(7, 0, '', 1, '0.00', 0, 1, 1, 1, '2016-06-29 08:56:02', '2016-06-29 08:56:02'),
(8, 0, '', 1, '0.00', 0, 1, 1, 1, '2016-06-29 08:56:17', '2016-06-29 08:56:17'),
(9, 78701, 'd72d8556-9385-46f8-9d94-18cab7f31912', 1, '0.00', 0, 1, 1, 1, '2016-06-29 08:58:56', '2016-06-29 08:58:56');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  `price` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `catalog_id`, `price`) VALUES
(1, 1, 15, '0.00'),
(2, 2, 15, '0.00'),
(3, 3, 15, '0.00'),
(4, 4, 15, '0.00'),
(5, 5, 15, '0.00'),
(6, 6, 15, '0.00'),
(7, 7, 15, '0.00'),
(8, 8, 15, '0.00'),
(9, 9, 15, '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `how_to_payment` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `name`, `how_to_payment`, `status`, `created`, `modified`) VALUES
(1, 'Transfer BCA', 'Please transfer to digilibs account name xxxxxx', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Transfer Mandiri', 'Please transfer to digilibs account name xxxxxx', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `key` char(50) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `key`, `value`, `status`, `created`, `modified`) VALUES
(43, 'Price Thesis', 'Price.Thesis', '200000', 1, NULL, NULL),
(44, 'Price Practical Work', 'Price.PracticalWork', '100000', 1, NULL, NULL),
(13, 'Link Facebook', 'Social.fb', 'http://www.facebook.com/', 1, '2016-04-28 09:03:56', '2016-04-28 09:03:58'),
(12, 'Link Twitter', 'Social.tw', 'http://www.twitter.com/', 1, '2016-04-28 09:03:51', '2016-04-28 09:03:54'),
(11, 'Link Instagram', 'Social.ig', 'http://www.instagram.com/', 1, '2016-04-28 09:03:46', '2016-04-28 09:03:49'),
(10, 'Tagline Website', 'Site.tagline', NULL, 1, NULL, NULL),
(9, 'Title Website', 'Site.title', 'Digilibs', 1, NULL, NULL),
(41, 'Meta Keyword', 'Meta.keywords', NULL, 1, NULL, NULL),
(42, 'Meta Description', 'Meta.description', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` char(40) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `group_id`, `created`, `modified`) VALUES
(1, 'admin', 'b9c3680cd62ac7e86664f9db2106e10f6fefdd6a', 1, '2016-04-11 09:05:55', '2016-04-19 10:48:41'),
(2, 'iqbalwaee', 'b9c3680cd62ac7e86664f9db2106e10f6fefdd6a', 1, '2016-04-19 11:00:29', '2016-04-19 11:00:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_phinxlog`
--
ALTER TABLE `acl_phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `acos`
--
ALTER TABLE `acos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`,`rght`),
  ADD KEY `alias` (`alias`);

--
-- Indexes for table `aros`
--
ALTER TABLE `aros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`,`rght`),
  ADD KEY `alias` (`alias`);

--
-- Indexes for table `aros_acos`
--
ALTER TABLE `aros_acos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `aro_id` (`aro_id`,`aco_id`),
  ADD KEY `aco_id` (`aco_id`);

--
-- Indexes for table `audits`
--
ALTER TABLE `audits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audit_log_phinxlog`
--
ALTER TABLE `audit_log_phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `catalogs`
--
ALTER TABLE `catalogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acos`
--
ALTER TABLE `acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `aros`
--
ALTER TABLE `aros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `aros_acos`
--
ALTER TABLE `aros_acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;
--
-- AUTO_INCREMENT for table `audits`
--
ALTER TABLE `audits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `catalogs`
--
ALTER TABLE `catalogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
