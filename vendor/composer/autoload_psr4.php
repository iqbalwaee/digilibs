<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Zend\\Validator\\' => array($vendorDir . '/zendframework/zend-validator/src'),
    'Zend\\Stdlib\\' => array($vendorDir . '/zendframework/zend-stdlib/src'),
    'Zend\\Hydrator\\' => array($vendorDir . '/zendframework/zend-hydrator/src'),
    'XdgBaseDir\\' => array($vendorDir . '/dnoegel/php-xdg-base-dir/src'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\VarDumper\\' => array($vendorDir . '/symfony/var-dumper'),
    'Symfony\\Component\\Filesystem\\' => array($vendorDir . '/symfony/filesystem'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Symfony\\Component\\Config\\' => array($vendorDir . '/symfony/config'),
    'Psy\\' => array($vendorDir . '/psy/psysh/src/Psy'),
    'Proffer\\' => array($vendorDir . '/davidyell/proffer/src'),
    'PhpParser\\' => array($vendorDir . '/nikic/php-parser/lib/PhpParser'),
    'PhpOffice\\PhpWord\\' => array($vendorDir . '/phpoffice/phpword/src/PhpWord'),
    'PhpOffice\\Common\\' => array($vendorDir . '/phpoffice/common/src/Common'),
    'Phinx\\' => array($vendorDir . '/robmorgan/phinx/src/Phinx'),
    'Migrations\\' => array($vendorDir . '/cakephp/migrations/src'),
    'DebugKit\\Test\\Fixture\\' => array($vendorDir . '/cakephp/debug_kit/tests/Fixture'),
    'DebugKit\\' => array($vendorDir . '/cakephp/debug_kit/src'),
    'Cake\\Test\\' => array($vendorDir . '/cakephp/cakephp/tests'),
    'Cake\\Composer\\' => array($vendorDir . '/cakephp/plugin-installer/src'),
    'Cake\\Chronos\\' => array($vendorDir . '/cakephp/chronos/src'),
    'Cake\\' => array($vendorDir . '/cakephp/cakephp/src'),
    'Bake\\' => array($vendorDir . '/cakephp/bake/src'),
    'Aura\\Intl\\_Config\\' => array($vendorDir . '/aura/intl/config'),
    'AuditStash\\' => array($vendorDir . '/lorenzo/audit-stash/src'),
    'Asika\\' => array($vendorDir . '/asika/pdf2text/src'),
    'App\\Test\\' => array($baseDir . '/tests'),
    'App\\' => array($baseDir . '/src'),
    'Acl\\' => array($vendorDir . '/cakephp/acl/src'),
);
